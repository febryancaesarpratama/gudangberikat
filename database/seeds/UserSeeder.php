<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => "admin",
            'email' => 'demoadmin@gmail.com',
            'password' => Hash::make('rahasia'),
            'login_name' => 'demoadmin',
            'is_active' => true,
            'email_verified_at' => date('Y-m-d H:i:s'),
            'language' => 'en',

        ]);
        DB::table('users')->insert([
            'first_name' => "super",
            'email' => 'superadmin@gmail.com',
            'password' => Hash::make('rahasia'),
            'login_name' => 'superadmin',
            'is_active' => true,
            'email_verified_at' => date('Y-m-d H:i:s'),
            'language' => 'en',
            'is_superadmin' => true,

        ]);
    }
}
