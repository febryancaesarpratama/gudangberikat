<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\ContentModel;

class SlugSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contents = ContentModel::where('slug','=',null)->get();
        if($contents->count() > 0){
            foreach ($contents as $v) {
                $slug = Str::slug($v->Title, '-');
                $update_slug = ContentModel::findOrFail($v->ID);
                $update_slug->slug = $slug;
                $update_slug->save();
            }
        }
    }
}
