<?php

use App\Models\ContentModel;
use App\Models\PageLayoutContentModel;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

use Illuminate\Support\Str;

class NewContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
            [
                'title' => 'Mengenal Lebih Dekat Tentang Gudang Berikat dan Perannya dalam Bisnis',
                'content' => 'Berbagai subjek penting berperan dalam operasional Gudang Berikat, suatu kawasan yang secara spesifik disediakan dan dikelola untuk kegiatan pengusahaan. Salah satu subjek kunci dalam hal ini adalah Penyelenggara Gudang Berikat. Penyelenggara ini, dengan izin yang ditetapkan oleh Direktur Jenderal atas nama Menteri Keuangan, bertanggung jawab atas penyediaan dan pengelolaan kawasan Gudang Berikat.. Izin ini berlaku selama lima tahun dan dapat diperpanjang. Subjek lain yang tak kalah penting adalah Pengusaha Gudang Berikat. Mereka juga melakukan kegiatan pengusahaan Gudang Berikat, dengan izin yang ditetapkan oleh Direktur Jenderal atas nama Menteri Keuangan, yang berlaku selama tiga tahun dan dapat diperpanjang. Terakhir, ada subjek unik yang disebut Pengusaha di Gudang Berikat merangkap Penyelenggara di Gudang Berikat (PDGB). Mereka ini memegang peran ganda dan menjadi bagian integral dari operasional Gudang Berikat. Subjek-subjek ini menjadikan operasional Gudang Berikat lebih efisien dan terorganisir.',
                'keyword' => 'Gudang Berikat, Penyelenggara Gudang Berikat, Pengusaha Gudang Berikat, PDGB',
            ],
            [
                'title' => 'Keuntungan dan Ketentuan Gudang Berikat di Indonesia',
                'content' => 'Gudang Berikat merupakan suatu fasilitas khusus yang ditunjuk dan diatur oleh Direktorat Jenderal Bea dan Cukai (DJBC). Dengan bangunan, tempat atau kawasan yang memenuhi standar tertentu, Gudang Berikat digunakan untuk menimbun barang impor dalam periode satu tahun. Gudang Berikat juga bisa digunakan untuk melakukan kegiatan operasional lainnya seperti pengemasan, penggabungan atau kitting, pengepakan, dan penyortiran barang impor. Selain itu, terdapat juga fasilitas layanan berupa kemudahan perizinan, operasional dan kepabeanan lainnya. Namun, jika kegiatan tersebut tidak sesuai, maka lebih cocoknya menjadi Kawasan Berikat. Dalam rangka pengawasan, Gudang Berikat juga dapat mengalami pemeriksaan pabean. Namun, pemeriksaan ini dilakukan secara selektif berdasarkan manajemen resiko dengan tujuan untuk memastikan kelancaran arus barang.',
                'keyword' => 'Gudang Berikat, Penyelenggara Gudang Berikat, Pengusaha Gudang Berikat, PDGB',
            ],
            [
                'title' => 'Mengenal Lebih Dekat tentang Gudang Berikat',
                'content' => 'Apabila Anda seringkali berurusan dalam industri impor, istilah Gudang Berikat pasti tak lagi asing. Fasilitas ini, yang dikelola oleh Direktorat Jenderal Bea dan Cukai (DJBC) dibawah naungan Menteri Keuangan Republik Indonesia, memiliki sejumlah keunggulan bagi para penggunanya. Gudang Berikat adalah space terukur yang berfungsi untuk penumpukan barang impor selama satu tahun sebelum dikeluarkan dengan hak penangguhan bea masuk. Ada rangkaian kegiatan yang dapat dilakukan, misalnya pengemasan, penggabungan, pengepakan, dan lainnya. Memilih untuk menggunakan fasilitas jasa lain seperti penyortiran atau pemotongan barang bersamaan dengan menimbun, maka Kawasan Berikat lebih sesuai. Selain dari fasilitas penangguhan bea masuk, terdapat beberapa kemudahan lain yang turut serta dimanfaatkan oleh pengguna Gudang Berikat seperti perkara perijinan, operasional, kepabeanan dan cukai. Meski biasanya berada dibawah pengawasan pabean, prosesnya berjalan selektif berdasarkan manajemen resiko dengan tujuan tetap melancarkan pergerakan barang.',
                'keyword' => 'Gudang Berikat, Penyelenggara Gudang Berikat, Pengusaha Gudang Berikat, PDGB',
            ],
            [
                'title' => 'Pemahaman Konsep dan Peran Subjek Dalam Gudang Berikat',
                'content' => 'Gudang Berikat merupakan wilayah yang diatur dalam peraturan pabean dimana barang-barang disimpan tanpa dikenakan bea masuk. Ada tiga subjek utama yang berperan dalam operasionalnya: Penyelenggara, Pengusaha, dan PDGB. Penyelenggara Gudang Berikat adalah pihak yang menyediakan dan mengelola area ini dengan izin dari Direktur Jenderal bagi Menteri Keuangan, yang berlaku selama lima tahun dan dapat diperpanjang. Pengusaha Gudang Berikat adalah entitas yang melakukan kegiatan operasional, dengan izin yang diberikan juga oleh Direktur Jenderal atas nama Menteri Keuangan dan berlaku selama tiga tahun dengan opsi perpanjangan. Kegiatan mereka mencakup berbagai aspek, seperti pengelolaan, penjualan, dan pengepakan barang-barang di gudang. PDGB atau Pengusaha di Gudang Berikat merangkap Penyelenggara di Gudang Berikat, merupakan subjek yang menjalankan dua peran sekaligus yaitu sebagai penyelenggara dan pengusaha. Mereka bertanggung jawab atas pengelolaan dan operasional gudang, memastikan semua operasional berjalan sesuai aturan pabean.',
                'keyword' => 'Gudang Berikat, Penyelenggara Gudang Berikat, Pengusaha Gudang Berikat, PDGB',

            ],
            [
                'title' => 'Persyaratan Pengajuan Fasilitas Gudang Berikat',
                'content' => 'Pengajuan Fasilitas Gudang Berikat merupakan kegiatan yang dapat dilakukan oleh pemilik bangunan, tempat, atau kawasan. Beberapa persyaratan diperlukan, salah satunya adalah lokasi yang dapat diakses langsung dari jalan umum dan dapat dilalui oleh kendaraan peti kemas. Syarat kedua adalah area tersebut harus memiliki batas yang jelas dengan pagar pemisah. Bangunan atau kawasan tersebut juga tidak boleh berhubungan langsung dengan bangunan lain dan memiliki satu pintu utama untuk keluar masuk barang. Selain itu, pelaku usaha harus melewati masa 10 tahun sejak menjalani hukuman pidana atau pailit jika pernah terlibat tindak pidana kepabeanan. Fasilitas yang diajukan ini digunakan untuk penumpukan barang-barang industri, distribusi ke Toko Bebas Bea, atau untuk ekspor.',
                'keyword' => 'Gudang Berikat, Penyelenggara Gudang Berikat, Pengusaha Gudang Berikat, PDGB',

            ],
            [
                'title' => 'Proses Permohonan Penetapan Gudang Berikat dan Penyelenggara Gudang Berikat',
                'content' => 'Perusahaan yang berkeinginan meraih status sebagai Gudang Berikat dan Penyelenggara Gudang Berikat harus melalui proses permohonan secara tertulis, baik bentuk softcopy maupun hardcopy kepada Direktur Jenderal melalui Kepala Kantor Pabean. Proses ini disertai dengan berbagai lampiran penting yang disyaratkan sesuai peraturan tentang Gudang Berikat. Lampiran yang dibutuhkan untuk Penetapan Gudang Berikat dan masing-masing izin (Penyelenggara, Pengusaha, atau PDGB) antara lain: surat izin tempat usaha, Izin Mendirikan Bangunan (IMB), bukti kepemilikan atau penguasaan tempat usaha, dan lain-lain. Jika tempat usaha disewa, Perjanjian Sewa minimal harus mencapai durasi lima tahun untuk Penyelenggara Gudang Berikat dan tiga tahun untuk Pengusaha Gudang Berikat atau PDGB. Sementara itu, untuk Pengusaha Gudang Berikat dan PDGB dibutuhkan tambahan lampiran seperti: fotokopi surat izin usaha perdagangan dan industri, fotokopi Kartu Angka Pengenal Impor (API), dan kontrak kerjasama dengan Toko Bebas Bea tujuan distribusi dan izin Toko Bebas Bea untuk Gudang Berikat Pusat Distribusi Toko Bebas Bea.',
                'keyword' => 'Gudang Berikat, Penyelenggara Gudang Berikat, Pengusaha Gudang Berikat, PDGB',

            ],
            [
                'title' => 'Optimalisasi Bisnis Impor Manfaat Strategis Menggunakan Fasilitas Gudang Berikat',
                'content' => '*Title:** Keunggulan Fasilitas Gudang Berikat dalam Bisnis Impor **Keywords:** Gudang Berikat, Fasilitas Gudang Berikat, Bea Masuk, Kepabeanan **Isi Konten:** Gudang Berikat menawarkan solusi efisien untuk pelaku bisnis impor dalam menangani masalah penyimpanan dan penanganan barang impor. Sebagai tempat penimbunan berikat, gudang ini tidak hanya memfasilitasi penyimpanan, tetapi juga pengemasan ulang, penyortiran, hingga pemotongan barang. Fasilitas ini memungkinkan barang impor dikelola dengan lebih efektif sebelum dipasarkan atau digunakan dalam proses produksi, sehingga menawarkan fleksibilitas operasional yang lebih besar bagi pengusaha. Selain kapasitas simpan yang optimal, Gudang Berikat juga memberikan manfaat utama berupa penangguhan bea masuk. Keuntungan ini memberi kemudahan finansial bagi pelaku usaha, memungkinkan mereka mengalokasikan dana untuk investasi atau kebutuhan operasional lain. Ditambah lagi, fasilitas ini dilengkapi dengan pelayanan perijinan yang lebih mudah,',
                'keyword' => 'Gudang Berikat, Penyelenggara Gudang Berikat, Pengusaha Gudang Berikat, PDGB',

            ],
            [   
                    'title' => 'Mengoptimalkan Arus Kas dengan Fasilitas Gudang Berikat Indonesia Panduan oleh Menteri Keuangan da',
                'content' => '**Title**: Memahami Fasilitas Gudang Berikat di Indonesia **Keywords**: Gudang Berikat, Menteri Keuangan Republik Indonesia, Direktorat Jenderal Bea dan Cukai, penangguhan Bea Masuk, fasilitas kepabeanan dan cukai **Content**: **Pengenalan Gudang Berikat di Indonesia** Gudang Berikat merupakan sebuah inisiatif oleh Menteri Keuangan Republik Indonesia, yang dikelola oleh Direktorat Jenderal Bea dan Cukai (DJBC). Konsep ini dirancang untuk memfasilitasi penimbunan barang impor selama satu tahun tanpa harus membayar Bea Masuk sejak awal. Ini memberikan kesempatan bagi perusahaan untuk mengatur arus kas mereka lebih efektif, sambil memberikan fleksibilitas dalam pengelolaan stok. **Fasilitas dan Manfaat Gudang Berikat** Selain dari penangguhan Bea Masuk, Gudang Berikat juga menawarkan sejumlah kemudahan lain, termasuk dalam pelayanan perijinan, operasional, serta aspek kepabeanan dan cukai. Fasilitas ini memungkinkan perusahaan melakukan kegiatan tambahan seperti pengemasan dan pengg',
                'keyword' => 'Gudang Berikat, Penyelenggara Gudang Berikat, Pengusaha Gudang Berikat, PDGB',
            ],
            [
                'title' => 'Mengoptimalkan Penyimpanan Barang Impor Melalui Fasilitas Gudang Berikat di Indonesia',
                'content' => '<div>
                <p><strong>Gudang Berikat: Solusi Efisien Penimbunan Barang Impor</strong><br>
                Sebagai upaya untuk mendukung efisiensi dan efektivitas dalam penanganan barang impor, Menteri Keuangan Republik Indonesia, melalui Direktorat Jenderal Bea dan Cukai (DJBC), menyediakan fasilitas berupa Gudang Berikat. Gudang Berikat merupakan bangunan, tempat, atau kawasan yang dengan syarat tertentu, diizinkan untuk menimbun barang impor selama 1 (satu) tahun sebagai cara untuk menunda pembayaran Bea Masuk. Konsep ini sesuai bagi perusahaan yang membutuhkan waktu dalam proses pengeluaran barang impor mereka.</p>
              
                <p><strong>Kegiatan di Gudang Berikat</strong><br>
                Tak hanya sebagai tempat menimbun, Gudang Berikat juga memungkinkan perusahaan untuk melakukan berbagai kegiatan pengolahan barang impor tanpa melanggar regulasi. Kegiatan tersebut mencakup pengemasan, pengemasan kembali, penyortiran, dan lain-lain. Aspek ini memberikan keluwesan bagi perusahaan untuk menyesuaikan produk impor sesuai kebutuhan pasar lokal atau persiapan ekspor ulang, sebelum akhirnya menuntaskan kewajiban Bea Masuk mereka. Namun, jika kegiatan pengolahan barang melampaui kegiatan yang disebutkan, perusahaan diharuskan menggunakan fasilitas Kawasan Berikat, bukan Gudang Berikat.</p>
              
                <p><strong>Manfaat Gudang Berikat bagi Perusahaan</strong><br>
                Selain penangguhan Bea Masuk, Gudang Berikat menawarkan berbagai kemudahan lain seperti pelayanan perizinan yang lebih cepat, kemudahan dalam operasional sehari-hari, dan proses kepabeanan lainnya yang lebih sederhana. Guna memastikan kepatuhan terhadap regulasi, pemeriksaan pabean dilakukan secara selektif berdasarkan manajemen risiko, memastikan bahwa tidak hanya arus barang yang lancar tetapi juga keamanan dan kepatuhan terhadap regulasi terjaga. Fasilitas ini menawarkan sebuah solusi yang memberikan manfaat besar bagi perusahaan yang bergerak di bidang impor, menciptakan efisiensi baik dalam waktu maupun biaya.</p>
              </div>',
                'keyword' => 'Gudang Berikat, Penyelenggara Gudang Berikat, Pengusaha Gudang Berikat, PDGB',
            ],
            [
                'title' => 'Optimalisasi Proses Logistik Internasional Melalui Fasilitas Gudang Berikat Manfaat dan Jenisnya',
                'content' => 'Gudang Berikat merupakan konsep penting dalam dunia perdagangan internasional. Sebagai tempat penimbunan berikat, fungsi utamanya adalah untuk barang impor yang memerlukan pengemasan ulang, penyortiran, penggabungan barang, pengepakan, penyetelan, hingga pemotongan. Keberadaan gudang berikat memungkinkan barang diproses dalam waktu tertentu sebelum dikeluarkan kembali ke pasaran. Fasilitas ini menjadi solusi bagi pelaku usaha untuk mengoptimalkan proses logistik dan distribusi barang dengan efisien.
                Fasilitas Gudang Berikat menawarkan sejumlah keuntungan bagi penggunanya, termasuk penangguhan bea masuk yang merupakan salah satu daya tarik utama. Selain itu, keberadaan fasilitas ini juga diiringi dengan kemudahan pelayanan perijinan, kegiatan operasional, dan akses kepada kemudahan kepabeanan dan cukai lainnya. Kemudahan-kemudahan ini secara langsung dapat mengurangi beban biaya dan waktu yang diperlukan dalam proses impor, sehingga meningkatkan efisiensi operasional untuk para pelaku industri dan perdagangan.
                Adapun jenis-jenis Gudang Berikat mencakup 1. Gudang Berikat Pendukung Kegiatan Industri, yang mendukung produksi industri dengan penyediaan bahan baku impor; 2. Gudang Berikat Pusat Distribusi Khusus Toko Bebas Bea, yang memfasilitasi distribusi barang untuk keperluan bebas bea; dan 3.Gudang Berikat Transit, yang digunakan sebagai tempat penampungan sementara bagi barang impor atau ekspor. Dengan demikian, keberadaan fasilitas ini tidak hanya mendukung kegiatan bisnis yang lebih lancar tetapi juga berkontribusi dalam peningkatan efisiensi dan efektivitas logistik barang dan jasa di tingkat internasional.',
                'keyword' => 'Gudang Berikat, Penyelenggara Gudang Berikat, Pengusaha Gudang Berikat, PDGB',
            ],
            [
                'title' => 'Konten Kriteria Penting dalam Memilih Gudang Berikat untuk Mendukung Kegiatan Industri dan Perdag',
                'content' => '<h2>Mengenal Syarat Gudang Berikat untuk Mendukung Industri</h2>
                <p>Sebuah bangunan atau tempat yang ingin diakui sebagai <strong>Gudang Berikat</strong> harus memenuhi kriteria tertentu yang ditetapkan oleh pihak berwenang. Kriteria ini bertujuan untuk memastikan bahwa lokasi tersebut memadai untuk menunjang kegiatan industri dan perdagangan internasional. Pertama, lokasi harus mudah diakses langsung dari jalan umum dan mampu dilalui kendaraan pengangkut peti kemas. Kedua, harus memiliki batas yang jelas terpisah dari bangunan atau tempat lain, seperti ditandai dengan pagar pemisah, untuk menjamin keamanan dan integritas barang yang disimpan.</p>
                <p>Selanjutnya, bangunan atau tempat tersebut tidak boleh berhubungan langsung dengan bangunan lain dan harus memiliki satu pintu utama untuk pemasukan dan pengeluaran barang. Kriteria ini dirancang untuk memastikan kontrol yang ketat terhadap pergerakan barang. Selain itu, diperlukan juga rekam jejak legal yang bersih, dimana perusahaan pengelola tidak terlibat dalam tindak pidana kepabeanan atau kebangkrutan selama sepuluh tahun terakhir. Ini menjamin bahwa operasional gudang dijalankan oleh entitas yang memiliki reputasi baik dan dapat dipercaya.</p>
                <p>Akhirnya, penggunaan <strong>Gudang Berikat</strong> sangat spesifik, yaitu hanya untuk penyimpanan barang yang mendukung kegiatan industri di daerah pabean atau Kawasan Berikat lainnya, distribusi ke Toko Bebas Bea, atau tujuan ekspor. Syarat-syarat ini, jika dipatuhi, tidak hanya memfasilitasi perdagangan internasional tetapi juga meningkatkan efisiensi dan keamanan dalam proses logistik industri di sebuah negara. Oleh karena itu, pemilihan dan persiapan bangunan atau area sebagai Gudang Berikat memegang peranan penting dalam mendukung infrastruktur perdagangan suatu negara.</p>',
                'keyword' => 'Gudang Berikat, Penyelenggara Gudang Berikat, Pengusaha Gudang Berikat, PDGB',
            ]
        ];

        foreach($data as $key){
            $content = ContentModel::create([
                'Title' => $key['title'],
                'ShortContent' => $key['content'] ?? 'Berikrat adalah sebuah platform yang menyediakan layanan pembuatan konten secara otomatis',
                'Content' => $key['content'],
                'slug' => Str::slug($key['title'], '-') ?? Str::slug('Berikrat adalah sebuah platform yang menyediakan layanan pembuatan konten secara otomatis', '-'),
                'keyword' => "keyword",
                'meta_description' => $key['title'],
                'IsPublished' => 1,
                'AddedTime' => Carbon::now()->timestamp ?? 0,
                'IDPrevArticle' => 0,
                'IDNextArticle' => 0,

            ]);


            // ContentMediaModel::create([
            //     'URL' => $imagereplace,
            //     'SortNo' => 1,
            //     'IDContent' => $content->ID,
            //     'IsPrimary' => 1,
            //     'AddedTime' => time(),
            //     'AddedByIP' => $request->ip(),
            //     'EditedTime' => time(),
            //     'EditedByIP' => $request->ip(),
            //     'Type' => 'Image'
            // ]);

            PageLayoutContentModel::create([
                'IDPage' => 2,
                'TableNo' => 13,
                'TableWidth' => NULL,
                'TableHeight' => NULL,
                'TableCSS' => 0,
                'RowNo' => 0,
                'RowWidth' => NULL,
                'RowHeight' => NULL,
                'RowCSS' => NULL,
                'ColNo' => 0,
                'ColWidth' => NULL,
                'ColHeight' => NULL,
                'ColCSS' => NULL,
                'IDContent' => $content->ID,
                'AddedTime' => Carbon::now()->timestamp,
                'AddedByIP' => '127.0.0.1',
                'EditedTime' => Carbon::now()->timestamp,
                'EditedByIP' => '127.0.0.1',
                'DivNo' => 13,
                'DivWidth' => 100,
                'DivHeight' => 40,
                'DivCss' => '',
                'DivClass' => 'col-md-12',

            ]);
        }
    }
}
