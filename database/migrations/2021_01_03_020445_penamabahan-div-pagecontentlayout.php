<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PenamabahanDivPagecontentlayout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pagelayoutcontents', function (Blueprint $table) {
            $table->integer('DivNo');
            $table->integer('DivWidth');
            $table->integer('DivHeight');
            $table->longText('DivCss');
            $table->longText('DivClass');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pagelayoutcontents', function($table) {
            $table->dropColumn('DivNo');
            $table->dropColumn('DivWidth');
            $table->dropColumn('DivHeight');
            $table->dropColumn('DivCss');
            $table->dropColumn('DivClass');
        });
    }
}
