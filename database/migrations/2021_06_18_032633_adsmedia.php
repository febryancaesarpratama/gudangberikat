<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Adsmedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adsmedia', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_media')->unsigned();
            $table->string('IsCover',1)->default('n');
            $table->mediumText('Location');
            $table->integer('SortNo');
            $table->string('IsPrimary',1)->default('n');
            $table->timestamps();

            $table->index(["id_media"], 'id_media');


            $table->foreign('id_media', 'id_media')
                ->references('id')->on('adsmanager')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('adsmanager', function (Blueprint $table) {
            $table->text('boostrap_class')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adsmedia');
        Schema::table('adsmanager', function (Blueprint $table) {
            // $table->dropUnique('slug');
            $table->dropColumn('boostrap_class');
        });
        
    }
}
