<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagelayoutcontentsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'pagelayoutcontents';

    /**
     * Run the migrations.
     * @table pagelayoutcontents
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('ID');
            $table->bigInteger('IDPage')->unsigned();
            $table->integer('TableNo');
            $table->integer('TableWidth')->nullable()->default(null);
            $table->integer('TableHeight')->nullable()->default(null);
            $table->mediumText('TableCSS')->nullable()->default(null);
            $table->integer('RowNo');
            $table->integer('RowWidth')->nullable()->default(null);
            $table->integer('RowHeight')->nullable()->default(null);
            $table->mediumText('RowCSS')->nullable()->default(null);
            $table->integer('ColNo');
            $table->integer('ColWidth')->nullable()->default(null);
            $table->integer('ColHeight')->nullable()->default(null);
            $table->mediumText('ColCSS')->nullable()->default(null);
            $table->integer('IDContent')->nullable()->default(null);
            $table->integer('AddedTime');
            $table->string('AddedByIP', 64);
            $table->integer('EditedTime')->nullable()->default(null);
            $table->string('EditedByIP', 64)->nullable()->default(null);

            $table->index(["IDPage"], 'pagelayoutcontents_ibfk_1');


            $table->foreign('IDPage', 'pagelayoutcontents_ibfk_1_content')
                ->references('ID')->on('pages')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
