<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagemenusTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'pagemenus';

    /**
     * Run the migrations.
     * @table pagemenus
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('ID');
            $table->bigInteger('IDPageLayoutContent')->unsigned();
            $table->bigInteger('IDPageDestination')->nullable()->default(null);
            // $table->bigInteger('IDPageDestination')->unsigned();
            $table->integer('Idx')->nullable()->default(null);
            $table->integer('AddedTime');
            $table->string('AddedByIP', 64);
            $table->integer('EditedTime')->nullable()->default(null);
            $table->string('EditedByIP', 64)->nullable()->default(null);

            // $table->index(["IDPageDestination"], 'pagemenus_ibfk_2');

            $table->unique(["IDPageLayoutContent", "IDPageDestination"], 'ContentDestination');


            $table->foreign('IDPageLayoutContent', 'ContentDestination')
                ->references('ID')->on('pagelayoutcontents')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            // $table->foreign('IDPageDestination', 'pagemenus_ibfk_2_pagemenus')
            //     ->references('ID')->on('pages')
            //     ->onDelete('cascade')
            //     ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
