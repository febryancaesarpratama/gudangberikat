<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OpmitasiMenuDinammis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pagemenus', function (Blueprint $table) {
            $table->bigInteger('IDPage')->nullable()->unsigned();
            $table->index('IDPage', 'pagemenus_id_page');

            $table->foreign('IDPage')
                ->references('id')
                ->on('pages')
                ->onDelete('cascade');
        });

        Schema::table('pages', function (Blueprint $table) {
            $table->text('description');
            $table->text('keyword');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
