<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PenyesuaianJadwalSholat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_kota', function (Blueprint $table) {
            $table->dropColumn('id');
        });
        Schema::table('master_kota', function (Blueprint $table) {
            $table->increments('id');
        });
        Schema::table('jadwal_sholat', function (Blueprint $table) {
            $table->string('midnight',255)->nullable();
            $table->integer('kota_external_id')->nullable()->change();
            $table->unsignedInteger('kota_id')->change();

            // $table->index(["kota_id"], 'jadwal_sholat_kota_id');

            // $table->foreign('kota_id', 'jadwal_sholat_kota_id_foreign')
            //     ->references('id')->on('master_kota');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_kota', function (Blueprint $table) {
            $table->integer('id')->changed();
        });
        Schema::table('jadwal_sholat', function (Blueprint $table) {
            $table->dropColumn('midnight');
            $table->integer('kota_external_id')->nullable(false)->changed();
            $table->dropForeign('jadwal_sholat_kota_id_foreign');
        });
    }
}


// alter table `jadwal_sholat` add constraint `jadwal_sholat_kota_id_foreign` foreign key (`kota_id`) references `master_kota` (`id`);
// ALTER TABLE `db_alayka3`.`jadwal_sholat` ADD CONSTRAINT fk_city_id FOREIGN KEY (`kota_id`) REFERENCES `db_alayka3`.`master_kota`(`id`);