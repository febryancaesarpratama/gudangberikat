<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class JadwalSholat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal_sholat', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('kota_id');
            $table->integer('kota_external_id'); // id kota sesuai dengan api sholat, karena nama kota internal dan api beda
            $table->date('tanggal');
            $table->longText('lat')->nullable();
            $table->longText('lng')->nullable();
            $table->integer('country_id')->default(1);
            $table->string('imsak');
            $table->string('subuh');
            $table->string('terbit');
            $table->string('dhuha');
            $table->string('dzuhur');
            $table->string('ashar');
            $table->string('maghrib');
            $table->string('isya');
            // $table->unique(['kota_id','country_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal_sholat');
    }
}
