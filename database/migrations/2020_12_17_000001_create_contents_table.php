<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'contents';

    /**
     * Run the migrations.
     * @table contents
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('ID');
            $table->string('Category', 100);
            $table->string('Title', 100)->nullable()->default(null);
            $table->string('ShortContent', 200)->nullable()->default(null);
            $table->text('Content')->nullable()->default(null);
            $table->mediumText('HeaderLocation')->nullable()->default(null);
            $table->mediumText('CenterLocation')->nullable()->default(null);
            $table->mediumText('FooterLocation')->nullable()->default(null);
            $table->integer('IDPrevArticle')->nullable()->default(null);
            $table->integer('IDNextArticle')->nullable()->default(null);
            $table->mediumText('SourceURL')->nullable()->default(null);
            $table->string('IsPublished', 1)->nullable()->default('n');
            $table->integer('AddedTime');
            $table->string('AddedByIP', 64);
            $table->integer('EditedTime')->nullable()->default(null);
            $table->string('EditedByIP', 64)->nullable()->default(null);
            $table->integer('TotalView');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
