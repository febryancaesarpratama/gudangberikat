<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddsManager extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adsmanager', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('Title');
            $table->string('ShortContent');
            $table->text('Content');
            $table->mediumText('Location');
            $table->string('IsPrimary',1)->default('n');
            $table->mediumText('DestinationURL');
            $table->bigInteger('IDContent')->unsigned();
            $table->datetime('StartPeriod');
            $table->datetime('EndPeriod');

            $table->string('Name');
            $table->string('Email');
            $table->string('PhoneNo');
            $table->string('Currency',3);
            $table->string('CurrencySign',10);
            $table->double('AdsFee');
            $table->string('IsVerified',1)->default('n');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adsmanager');
    }
}
