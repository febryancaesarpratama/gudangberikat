<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentreactionsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'contentreactions';

    /**
     * Run the migrations.
     * @table contentreactions
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('ID');
            $table->bigInteger('IDContent')->unsigned();
            $table->char('IsLove', 1)->nullable()->default('n');
            $table->char('IsLike', 1)->nullable()->default('n');
            $table->char('IsHaha', 1)->nullable()->default('n');
            $table->char('IsCare', 1)->nullable()->default('n');
            $table->char('IsSad', 1)->nullable()->default('n');
            $table->char('IsAngry', 1)->nullable()->default('n');
            $table->char('IsShare', 1)->nullable()->default('n');
            $table->string('ShareTo', 30)->nullable()->default(null);
            $table->integer('AddedTime');
            $table->string('AddedByIP', 64);
            $table->integer('EditedTime')->nullable()->default(null);
            $table->string('EditedByIP', 64)->nullable()->default(null);

            $table->index(["IDContent"], 'IDContent');


            $table->foreign('IDContent', 'IDContent')
                ->references('ID')->on('contents')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
