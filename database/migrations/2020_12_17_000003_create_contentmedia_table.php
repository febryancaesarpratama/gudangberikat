<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentmediaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'contentmedia';

    /**
     * Run the migrations.
     * @table contentmedia
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('ID');
            $table->bigInteger('IDContent')->unsigned();
            $table->string('IsCover', 1)->nullable()->default('n');
            $table->mediumText('URL')->nullable()->default(null);
            $table->integer('Width')->nullable()->default(null);
            $table->integer('Height')->nullable()->default(null);
            $table->integer('SortNo');
            $table->string('IsPrimary', 1)->default('n');
            $table->string('Type', 30)->nullable()->default(null);
            $table->integer('AddedTime');
            $table->string('AddedByIP', 64);
            $table->integer('EditedTime')->nullable()->default(null);
            $table->string('EditedByIP', 64)->nullable()->default(null);

            $table->index(["IDContent"], 'IDContent');


            $table->foreign('IDContent', 'IDContentMedia')
                ->references('ID')->on('contents')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
