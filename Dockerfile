FROM php:7.2.29-apache
RUN a2enmod rewrite
EXPOSE 80
COPY ./ /app
COPY ./.docker/php7-2/vhost.conf /etc/apache2/sites-available/000-default.conf
RUN chown -R www-data:www-data /app