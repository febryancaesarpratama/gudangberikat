<?php
use Illuminate\Support\Facades\Route;
use Spatie\Sitemap\SitemapGenerator;
use App\Models\PageModel;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Front\DynamisController@index');
Route::get('/home','Front\DynamisController@index');

Route::get('/contac_us', function () {
    return view('contact_us');
});
Route::get('/demo-page',function(){
    return view('layout.master');
});
Route::get('/test-front','Front\DynamisController@index');
Route::get('/content/{id}','Front\DynamisController@contentDetail')->name('front.content.detail');
// Route::get('/content/{id}','Front\DynamisController@contentDetail')->name('front.content.detail');
Route::get('/test-curl','JadwalSholatController@getJadwalSholat');



Route::get("generate-sitemap", function () {

    SitemapGenerator::create("https://itinventorybeacukai.com/")->writeToFile(public_path('sitemap.xml'));
    dd("done");
});


Route::prefix('/admin')->name('admin.')->namespace('Admin')->group(function(){
    
    /**
     * Admin Auth Route(s)
     */
    Route::namespace('Auth')->group(function(){
        
        //Login Routes
        Route::get('/login','LoginController@showLoginForm')->name('login');
        Route::post('/login','LoginController@login')->name('login');
        Route::post('/logout','LoginController@logout')->name('logout');

        //Register Routes
        // Route::get('/register','RegisterController@showRegistrationForm')->name('register');
        // Route::post('/register','RegisterController@register');

        //Forgot Password Routes
        // Route::get('/password/reset','ForgotPasswordController@showLinkRequestForm')->name('password.request');
        // Route::post('/password/email','ForgotPasswordController@sendResetLinkEmail')->name('password.email');

        //Reset Password Routes
        // Route::get('/password/reset/{token}','ResetPasswordController@showResetForm')->name('password.reset');
        // Route::post('/password/reset','ResetPasswordController@reset')->name('password.update');

        // Email Verification Route(s)
        // Route::get('email/verify','VerificationController@show')->name('verification.notice');
        // Route::get('email/verify/{id}','VerificationController@verify')->name('verification.verify');
        // Route::get('email/resend','VerificationController@resend')->name('verification.resend');
        // Route::get('/email/verify/{id}','VerificationController@verify')->name('verification.verify');

    });

    Route::namespace('User')->group(function(){
    	Route::resource('user', 'UserManagementController')->middleware('auth:admin');
    	Route::resource('permission', 'PermissionController')->middleware('auth:admin');
        Route::resource('role', 'RoleController')->middleware('auth:admin');
        Route::resource('content', 'ContentController');

    });
    Route::namespace('Ads')->group(function(){
        Route::resource('adsmanager', 'AdsManagerController');

    });
    Route::namespace('Page')->group(function(){
        Route::resource('page', 'PageController');
        Route::delete('page/delete_menu/{id}','PageController@destroyMenu');
        Route::get('pagelayoutcontent/{id}','PageLayoutContentController@index')->name('pagelayoutcontent.index');
        Route::get('pagelayoutcontent/{id}/create','PageLayoutContentController@create')->name('pagelayoutcontent.create');
        Route::get('pagelayoutcontent/{id}/edit','PageLayoutContentController@edit')->name('pagelayoutcontent.edit');
        Route::patch('pagelayoutcontent/update','PageLayoutContentController@update')->name('pagelayoutcontent.update');
        Route::get('pagelayoutcontent/{id}/destroy','PageLayoutContentController@destroy')->name('pagelayoutcontent.destroy');
        Route::post('pagelayoutcontent/store','PageLayoutContentController@store')->name('pagelayoutcontent.store');

    });

    Route::get('/sukses',function(){
        return "sukses login";
    })->name('sukses');
});

$paths = ['/it','/about_us','/services','/products','/clients','/supports'];
// Route::get('/jadwalsholat','Front\DynamisController@pencarianJadwalSholat');
Route::get('/jadwalsholat','Front\DynamisController@indexSchedule');
Route::get('/generate-ai/artikel', 'Front\DynamisController@getArtikel');

try {
    $pages = PageModel::get();
    foreach ($pages as $page) {
        $paths[] = $page->URL;
    }
} catch (Exception $e) {
    echo '*************************************' . PHP_EOL;
    echo 'Error fetching database pages: ' . PHP_EOL;
    echo $e->getMessage() . PHP_EOL;
    echo '*************************************' . PHP_EOL;
}

foreach($paths as $path){
    $router->get($path, 'Front\DynamisController@index');
    $router->get($path."/{kota_id}", 'Front\DynamisController@index_kota');
    // $router->get($path."/{kota_id}/{tanggal}", 'Front\DynamisController@index_kota_tanggal');
}