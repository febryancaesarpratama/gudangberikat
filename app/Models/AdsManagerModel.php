<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdsManagerModel extends Model
{
    protected $table = "adsmanager";
	protected $primaryKey = "id";
	// public $timestamps = false;

	protected $fillable = [
        'Title','ShortContent','Content','Location','IsPrimary','DestinationURL','IDContent',
        'StartPeriod','EndPeriod','Name','Email','PhoneNo','Currency','CurrencySign','AdsFee',
        'IsVerified','boostrap_class'
	];

    public function media(){
        return $this->hasOne('App\Models\AdsMediaModel','id_media','id');
    }
}