<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterProvinsi extends Model
{
    protected $table = "master_provinsi";
	protected $primaryKey = "id";
	public $timestamps = false;
}
