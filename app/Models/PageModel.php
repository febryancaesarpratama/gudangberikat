<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageModel extends Model
{
    
    protected $table = 'pages';
    protected $primaryKey = "ID";
	public $timestamps = false;
    protected $fillable = [
        'Name',
        'URL',
        'AddedTime',
        'AddedByIP',
        'EditedTime',
        'EditedByIP',
        'description',
        'keyword'
    ];

    public function contentLayouts(){
        return $this->hasMany('App\Models\PageLayoutContentModel','IDPage','ID')->orderBy('RowNo','ASC');
    }

    public function menus(){
        return $this->hasMany('App\Models\PageMenuModel','IDPage','ID');
    }
}
