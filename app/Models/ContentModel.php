<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ContentModel extends Model
{
    protected $table = "contents";
	protected $primaryKey = "ID";
	public $timestamps = false;

	protected $fillable = [
        'Category', 'Title', 'ShortContent', 'Content', 'HeaderLocation', 'CenterLocation','FooterLocation','IDPrevArticle','IDNextArticle','SourceURL','IsPublished','AddedTime','AddedByIP','EditedTime','EditedByIP','TotalView','slug','keyword','meta_description'
	];
	
	public function contentMedias()
    {
        return $this->hasMany(ContentMediaModel::class, 'IDContent');
	}
	
	public function contentMedia()
    {
        return $this->hasOne(ContentMediaModel::class, 'IDContent');
    }

	public function getNextPrevArticle($param)
	{
		return $this->findOrFail($param);
	}

	
	public function getImage($id)
	{
		$contentMedia = \App\Models\ContentMediaModel::where('IDContent', $id)->where('Type', 'image')->where('IsCover', 'y')->orderBy('ID', 'DESC')->first();
		if ($contentMedia != null) {
			return asset('assets/img/blog/thumb/md_'.$contentMedia->Location);
		} else {
			return asset('assets/img/300x300.png');
		}
	}

	public function getFullImage($id)
	{
		$contentMedia = \App\Models\ContentMediaModel::where('IDContent', $id)->where('Type', 'image')->where('IsCover', 'y')->orderBy('ID', 'DESC')->first();

		if ($contentMedia != null) {
			return asset('assets/img/blog/'.$contentMedia->Location);
		} else {
			return asset('assets/img/1280x848.png');
		}
    }
    public function getAllImage($id)
	{
		$contentMedia = \App\Models\ContentMediaModel::where('IDContent', $id)->where('Type', 'Image')->orderBy('ID', 'DESC')->get();
		
		if ($contentMedia) {
			return $contentMedia;
		} else {
			return asset('assets/img/300x300.png');
		}
	}

	public function getAllVideo($id)
	{
		$contentMedia = \App\Models\ContentMediaModel::where('IDContent', $id)->where('Type', 'video')->orderBy('ID', 'DESC')->get();
		
		if ($contentMedia) {
			return $contentMedia;

		} else {
			return asset('assets/img/300x300.png');
		}
	}

	public function getAllPdf($id)
	{
		$contentMedia = \App\Models\ContentMediaModel::where('IDContent', $id)->where('Type', 'document')->orderBy('ID', 'DESC')->get();
		
		if ($contentMedia) {
			return $contentMedia;

		} else {
			return null;
		}
	}
}
