<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageMenuModel extends Model
{
    protected $table = 'pagemenus';
    protected $primaryKey = "ID";
	public $timestamps = false;
    protected $fillable = [
        'IDPageLayoutContent',
        'IDPageLayoutContent',
        'Idx',
        'AddedTime',
        'AddedByIP',
        'EditedTime',
        'EditedByIP',
        'menu_name',
        'menu_url',
        'IDPage',
        'is_default'
    ];
}