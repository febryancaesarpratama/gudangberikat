<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageLayoutContentModel extends Model
{
    protected $table = 'pagelayoutcontents';
    protected $primaryKey = "ID";
	public $timestamps = false;
    protected $fillable = [
        'IDPage',
        'TableNo',
        'TableWidth',
        'TableHeight',
        'TableCSS',
        'RowNo',
        'RowWidth',
        'RowHeight',
        'RowCSS',
        'ColNo',
        'ColWidth',
        'ColHeight',
        'ColCSS',
        'IDContent',
        'AddedTime',
        'AddedByIP',
        'EditedTime',
        'EditedByIP',
        'DivNo',
        'DivWidth',
        'DivHeight',
        'DivCss',
        'DivClass',
    ];

    public function content(){
        return $this->hasOne('App\Models\ContentModel','ID','IDContent');
    }
    public function page(){
        return $this->hasOne('App\Models\PageModel','ID','IDPage');
    }
}
