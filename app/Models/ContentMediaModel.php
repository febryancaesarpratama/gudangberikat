<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ContentMediaModel extends Model
{
    protected $table = "contentmedia";
	protected $primaryKey = "ID";
	public $timestamps = false;

	protected $fillable = [
        'IDContent', 'IsCover', 'URL', 'Width', 'Height', 'SortNo','IsPrimary','Type','AddedTime','AddedByIP','EditedTime','EditedByIP'
	];

    /**
     * content
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contents()
    {
        return $this->belongsTo(ContentModel::class, 'IDContent');
    }

    public function getAllVideoPath($id)
	{
		$contentMedia = \App\Models\ContentMedia::where('ID', $id)->where('Type', 'video')->orderBy('ID', 'DESC')->first();
		if ($contentMedia) {
                $contents = collect(Storage::cloud()->listContents('/', false));
					
                $getFile = $contents->where('name', '=', $contentMedia->Location)->first();
				
				$path = $getFile['path'];
		
				$data = 'https://drive.google.com/uc?export=download&id=' . $path;
				return $data;

		} else {
			return asset('assets/img/300x300.png');
		}
    }
    
    public function getAllPdfPath($id)
	{
		$contentMedia = \App\Models\ContentMedia::where('ID', $id)->where('Type', 'document')->orderBy('ID', 'DESC')->first();
		if ($contentMedia) {
                $contents = collect(Storage::cloud()->listContents('/', false));
					
                $getFile = $contents->where('name', '=', $contentMedia->Location)->first();
				
				$path = $getFile['path'];
		
				$data = 'https://drive.google.com/uc?export=download&id=' . $path;
				return $data;

		} else {
			return asset('assets/img/300x300.png');
		}
	}
}
