<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JadwalSholatModel extends Model
{
    protected $table = "jadwal_sholat";
	protected $primaryKey = "id";
	public $timestamps = false;

	protected $fillable = [
        'kota_id','kota_external_id','tanggal','lat','lng','country_id','imsak','subuh','terbit','dhuha','dzuhur','ashar','maghrib','isya'
	];
}