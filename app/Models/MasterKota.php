<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterKota extends Model
{
    protected $table = "master_kota";
	protected $primaryKey = "id";
	public $timestamps = false;
}
