<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterKecamatan extends Model
{
    protected $table = "master_kecamatan";
	protected $primaryKey = "id";
	public $timestamps = false;
}
