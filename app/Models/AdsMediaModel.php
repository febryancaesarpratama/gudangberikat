<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdsMediaModel extends Model
{
    protected $table = "adsmedia";
	protected $primaryKey = "id";
	// public $timestamps = false;

	protected $fillable = [
        'id_media','IsCover','Location','SortNo','IsPrimary'
	];

}
