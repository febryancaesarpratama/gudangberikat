<?php

namespace App\Http\Controllers\Admin\Page;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PageModel;
use App\Models\PageMenuModel;
use Spatie\Permission\Models\Permission;
use DB;
use DataTables;

class PageController extends Controller
{
    public function __construct()
    {
        $this->title = 'Page';
    }

    public function index(Request $request){
        $title = $this->title;

        if($request->ajax())
        {
            $data = PageModel::orderBy('ID','DESC')->get();
            
            return DataTables::of($data)
                ->addColumn('action', function($data){
                    $button = '<div class="btn-group m-t-15">';
                        $button .= '<button type="button" class="btn btn-sm btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Action</button>';
                        $button .='<div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">';
                            $button .= '<a class="dropdown-item" href="'.route('admin.pagelayoutcontent.index', $data->ID).'">Page Layout</a>';
                            $button .= '<a class="dropdown-item" href="'.route('admin.page.edit', $data->ID).'"><i class="fa fa-pencil"></i> Edit</a>';
                            $button .= '<a class="dropdown-item delete" href="#" id="'.$data->ID.'"><i class="fa fa-trash"></i> Delete</a>';
                        $button .= '</div>';
                    $button .= '</div>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.page.index', compact('title'));
    }

    public function create()
    {
        $title = $this->title;
        return view('admin.page.create', compact('title'));
    }

    public function store(Request $request)
    {
        $this->validator($request);
        
        \DB::beginTransaction();
        try {
            $input = ['Name' => $request->input('Name'), 'URL' => $request->input('URL'), 'AddedByIP'=>'127.0.0.1','AddedTime'=>strtotime(date('Y-m-d H:i:s'))];
            if($request->has('description') && $request->description !=''){
                $input['description'] = $request->description;
            }
            if($request->has('keyword') && $request->keyword !=''){
                $input['keyword'] = $request->keyword;
            }
            $role = PageModel::create($input);
            
            if ($role) {
                if($request->has('menu_items') && count($request->menu_items) > 0){
                    $affected = DB::table('pagemenus')->update(['is_default' => 0]);
                    foreach($request->menu_items as $item){
                        $input_menu_item = [
                            'IDPage' => $role->ID,
                            'menu_name' => $item['menu_name'],
                            'menu_url' => $item['menu_url'],
                        ];
                        if($request->has('menu_default')){
                            $input_menu_item['is_default'] = 1;
                        }
                        $menu_item = PageMenuModel::create($input_menu_item);
                    }
                }
                $redirect = redirect()->route('admin.page.index');
                back()->with('success',trans('message.create.success'));
                \DB::commit();
                return $redirect;
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            dd($message);
            \DB::rollback();
            back()->with('error',trans('message.create.error') . ' : ' . $message);
            return redirect()->back()->withInput();
        }
    }

    public function edit($id)
    {
        $title = $this->title;
        $listdata = PageModel::find($id);
        $permission = Permission::get()->sortBy('Name');
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();
        $menu_items = PageMenuModel::where(['IDPage'=>$id])->get()->sortBy('ID');
        $is_default = (!$menu_items->isEmpty())?$menu_items[0]->is_default:0;

        return view('admin.page.edit',compact('listdata','permission','rolePermissions','title','menu_items','is_default'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'Name' => 'required|unique:roles,Name,'.$id,
        ]);
        \DB::beginTransaction();
        try {

            $update = PageModel::find($id);
            $update->Name = $request->input('Name');
            if($request->input('URL')!=""){
                $update->URL = $request->input('URL');
            }
            if($request->has('description') && $request->description !=''){
                $update->description = $request->description;
            }
            if($request->has('keyword') && $request->keyword !=''){
                $update->keyword = $request->keyword;
            }
            $update->EditedTime = strtotime(date('Y-m-d H:i:s'));
            $update->EditedByIP = '127.0.0.1';
            $update->save();

            if ($update) {
                if($request->has('menu_items') && count($request->menu_items) > 0){
                    $affected = DB::table('pagemenus')->update(['is_default' => 0]);
                    DB::table("pagemenus")->where('IDPage',$id)->delete();
                    foreach($request->menu_items as $item){
                        $input_menu_item = [
                            'IDPage' => $id,
                            'menu_name' => $item['menu_name'],
                            'menu_url' => $item['menu_url'],
                        ];
                        if($request->has('menu_default')){
                            $input_menu_item['is_default'] = 1;
                        }
                        $menu_item = PageMenuModel::create($input_menu_item);
                    }
                }
                back()->with('success',trans('message.update.success'));
                \DB::commit();
                return redirect()->route( 'admin.page.index' );
            }
        } catch (\Exception $e) {
            \DB::rollback();
            back()->with('error',trans('message.update.error') . ' : ' . $e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function destroy($id, Request $request)
    {
        DB::table("pages")->where('id',$id)->delete();
        back()->with('success',trans('message.delete.success'));
    }

    public function destroyMenu($id,Request $request){
        DB::table("pagemenus")->where('ID',$id)->delete();
        back()->with('success',trans('message.delete.success'));
    }

    private function validator(Request $request)
    {
        $rules = [
            'Name' => 'required|unique:pages,Name',
        ];

        $messages = [];

        $request->validate($rules,$messages);
    }
}
