<?php

namespace App\Http\Controllers\Admin\Page;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PageLayoutContentModel;
use App\Models\ContentModel;
use App\Models\PageMenuModel;
use Spatie\Permission\Models\Permission;
use DB;
use DataTables;
use Auth;

class PageLayoutContentController extends Controller
{
    public function __construct()
    {
        $this->title = 'Page Layout Content';
    }

    public function index(Request $request,$id){
        $title = $this->title;

        if($request->ajax())
        {
            $data = PageLayoutContentModel::query()
            ->selectRaw("pagelayoutcontents.ID as ID, pagelayoutcontents.TableNo as TableNo, contents.Title as Title")
            ->join('contents','contents.ID','=','pagelayoutcontents.IDContent')
            ->where('pagelayoutcontents.IDPage',$id)->orderBy('pagelayoutcontents.TableNo','ASC')->get();
            return DataTables::of($data)
                ->addColumn('action', function($data){
                    $button = '<div class="btn-group m-t-15">';
                        $button .= '<button type="button" class="btn btn-sm btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Action</button>';
                         $button .='<div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">';
                            $button .= '<a class="dropdown-item" href="'.route('admin.pagelayoutcontent.edit', $data->ID).'"><i class="fa fa-pencil"></i> Edit</a>';
                            $button .= '<a class="dropdown-item delete" href="'.route('admin.pagelayoutcontent.destroy', $data->ID).'" id="'.$data->ID.'"><i class="fa fa-trash"></i> Delete</a>';
                        $button .= '</div>';
                    $button .= '</div>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.pagelayoutcontent.index', compact('title','id'));
    }

    public function create($id)
    {
        $title = $this->title;
        $contents = ContentModel::pluck('Title','ID')->all();
        return view('admin.pagelayoutcontent.create', compact('title','id','contents'));
    }

    public function store(Request $request)
    {
        \DB::beginTransaction();
        try {
            
            $input = ['IDPage' => $request->input('IDPage'),'IDContent'=>$request->input('IDContent'),'TableCSS'=>'0','RowNo'=>0,'ColNo'=>0,'AddedByIP'=>'127.0.0.1','AddedTime'=>strtotime(date('Y-m-d H:i:s'))];

            if($request->has('TableNo') && $request->TableNo !=""){
                $input['TableNo'] = $request->TableNo;
            }
            if($request->has('TableWidth') && $request->TableWidth !=""){
                $input['TableWidth'] = $request->TableWidth;
            }
            if($request->has('TableHeight') && $request->TableHeight !=""){
                $input['TableHeight'] = $request->TableHeight;
            }
            if($request->has('TableCSS') && $request->TableCSS !=""){
                $input['TableCSS'] = $request->TableCSS;
            }
            if($request->has('RowNo') && $request->RowNo !=""){
                $input['RowNo'] = $request->RowNo;
            }
            if($request->has('RowWidth') && $request->RowWidth !=""){
                $input['RowWidth'] = $request->RowWidth;
            }
            if($request->has('RowHeight') && $request->RowHeight !=""){
                $input['RowHeight'] = $request->RowHeight;
            }
            if($request->has('RowCSS') && $request->RowCSS !=""){
                $input['RowCSS'] = $request->RowCSS;
            }
            if($request->has('ColNo') && $request->ColNo !=""){
                $input['ColNo'] = $request->ColNo;
            }
            if($request->has('ColWidth') && $request->ColWidth !=""){
                $input['ColWidth'] = $request->ColWidth;
            }
            if($request->has('ColHeight') && $request->ColHeight !=""){
                $input['ColHeight'] = $request->ColHeight;
            }
            if($request->has('ColCSS') && $request->ColCSS !=""){
                $input['ColCSS'] = $request->ColCSS;
            }
            if($request->has('DivCss') && $request->DivCss !== ''){
                $input['DivCss'] = strip_tags($request->DivCss);
            }
            if($request->has('DivClass') && $request->DivClass !== ''){
                $input['DivClass'] = $request->DivClass;
            }            
            if($request->has('DivNo') && $request->DivNo !=""){
                $input['DivNo'] = $request->DivNo;
            }
            if($request->has('DivWidth') && $request->DivWidth !== ''){
                $input['DivWidth'] = strip_tags($request->DivWidth);
            }
            if($request->has('DivHeight') && $request->DivHeight !== ''){
                $input['DivHeight'] = $request->DivHeight;
            }        
            $role = PageLayoutContentModel::create($input);
            if ($role) {
                if($request->has('menu_items') && count($request->menu_items) > 0){
                    foreach($request->menu_items as $item){
                        $input_menu_item = [
                            'IDPageLayoutContent' => $role->ID,
                            'menu_name' => $item['menu_name'],
                            'menu_url' => $item['menu_url'],
                        ];
                        $menu_item = PageMenuModel::create($input_menu_item);
                    }
                }
                $redirect = redirect()->route('admin.pagelayoutcontent.index',$request->IDPage);
                back()->with('success',trans('message.create.success'));
                \DB::commit();
                return $redirect;
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            \DB::rollback();
            back()->with('error',trans('message.create.error') . ' : ' . $e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = $this->title;
        $listdata = PageLayoutContentModel::find($id);
        $permission = Permission::get()->sortBy('Name');
        $menu_items = PageMenuModel::where(['IDPagelayoutContent'=>$id])->get()->sortBy('ID');
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();
        $contents = ContentModel::pluck('Title','ID')->all();
        return view('admin.pagelayoutcontent.edit',compact('listdata','menu_items','permission','rolePermissions','title', 'contents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request);
        \DB::beginTransaction();
        try {
            $input = ['IDContent'=>$request->input('IDContent'),'EditedByIP'=>'127.0.0.1','EditedTime'=>strtotime(date('Y-m-d H:i:s'))];
            $update = PageLayoutContentModel::find($request->input('ID'));
            if($request->has('TableNo') && $request->TableNo !=""){
                $input['TableNo'] = $request->TableNo;
            }
            if($request->has('TableWidth') && $request->TableWidth !=""){
                $input['TableWidth'] = $request->TableWidth;
            }
            if($request->has('TableHeight') && $request->TableHeight !=""){
                $input['TableHeight'] = $request->TableHeight;
            }
            if($request->has('TableCSS') && $request->TableCSS !=""){
                $input['TableCSS'] = $request->TableCSS;
            }
            if($request->has('RowNo') && $request->RowNo !=""){
                $input['RowNo'] = $request->RowNo;
            }
            if($request->has('RowWidth') && $request->RowWidth !=""){
                $input['RowWidth'] = $request->RowWidth;
            }
            if($request->has('RowHeight') && $request->RowHeight !=""){
                $input['RowHeight'] = $request->RowHeight;
            }
            if($request->has('RowCSS') && $request->RowCSS !=""){
                $input['RowCSS'] = $request->RowCSS;
            }
            if($request->has('ColNo') && $request->ColNo !=""){
                $input['ColNo'] = $request->ColNo;
            }
            if($request->has('ColWidth') && $request->ColWidth !=""){
                $input['ColWidth'] = $request->ColWidth;
            }
            if($request->has('ColHeight') && $request->ColHeight !=""){
                $input['ColHeight'] = $request->ColHeight;
            }
            if($request->has('ColCSS') && $request->ColCSS !=""){
                $input['ColCSS'] = $request->ColCSS;
            }
            if($request->has('DivCss') && $request->DivCss !== ''){
                $input['DivCss'] = strip_tags($request->DivCss);
            }
            if($request->has('DivClass') && $request->DivClass !== ''){
                $input['DivClass'] = $request->DivClass;
            }            
            if($request->has('DivNo') && $request->DivNo !=""){
                $input['DivNo'] = $request->DivNo;
            }
            if($request->has('DivWidth') && $request->DivWidth !== ''){
                $input['DivWidth'] = strip_tags($request->DivWidth);
            }
            if($request->has('DivHeight') && $request->DivHeight !== ''){
                $input['DivHeight'] = $request->DivHeight;
            }
            if($request->has('menu_items') && count($request->menu_items) > 0){
                // dd($request->menu_items);
                DB::table("pagemenus")->where('IDPageLayoutContent',$request->input('ID'))->delete();
                
                foreach($request->menu_items as $item){
                    $input_menu_item = [
                        'IDPageLayoutContent' => $request->input('ID'),
                        'menu_name' => $item['menu_name'],
                        'menu_url' => $item['menu_url'],
                    ];
                    $menu_item = PageMenuModel::create($input_menu_item);
                }
            }
            $update->update($input);

            if ($update) {
                back()->with('success',trans('message.update.success'));
                \DB::commit();
                return redirect()->route('admin.pagelayoutcontent.index',$request->IDPage);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            back()->with('error',trans('message.update.error') . ' : ' . $e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("pagemenus")->where('IDPageLayoutContent',$id)->delete();
        $data = PageLayoutContentModel::findOrFail($id);
        $idpage = $data->IDPage;
        $data->delete();
        back()->with('success',trans('message.delete.success'));
        return redirect()->route('admin.pagelayoutcontent.index',$idpage);
    }
}
