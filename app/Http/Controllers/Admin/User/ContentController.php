<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ContentModel;
use App\Models\ContentMediaModel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB;
use DataTables;
use Carbon\Carbon;

class ContentController extends Controller
{
    public function __construct()
    {
        $this->title = 'Content';
    }

	
	 public function index(Request $request){
        $title = $this->title;

        if($request->ajax())
        {
            $data = ContentModel::orderBy('id','DESC')->get();
            
            return DataTables::of($data)
                ->addColumn('action', function($data){
                    $button = '<div class="btn-group m-t-15">';
                        $button .= '<button type="button" class="btn btn-sm btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Action</button>';
                        $button .='<div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">';
                            $button .= '<a class="dropdown-item" href="'.route('admin.content.edit', $data->ID).'"><i class="fa fa-pencil"></i> Edit</a>';
                            $button .= '<a class="dropdown-item delete" href="#" id="'.$data->ID.'"><i class="fa fa-trash"></i> Delete</a>';
                        $button .= '</div>';
                    $button .= '</div>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.content.index', compact('title'));
    }

    public function create()
    {
        $title = $this->title;
        $listContents = ContentModel::pluck('Title','Title')->all();
        return view('admin.content.create', compact('title','listContents'));
    }

    public function store(Request $request)
    {
        $this->validator($request);
        
        \DB::beginTransaction();
        try {
            $input = ['Title' => $request->input('Title'),'slug'=>Str::slug($request->input('Title'), '-')];
            if($request->has('ShortContent') && $request->ShortContent !== ''){
                $input['ShortContent'] = strip_tags($request->ShortContent);
            }
            if($request->has('Content') && $request->Content !== ''){
                $input['Content'] = $request->Content;
            }
            if($request->has('IDPrevArticle') && $request->IDPrevArticle !== ''){
                $input['IDPrevArticle'] = $request->IDPrevArticle;
            }
            if($request->has('IDNextArticle') && $request->IDNextArticle !== ''){
                $input['IDNextArticle'] = $request->IDNextArticle;
            }
            if($request->has('SourceURL') && $request->SourceURL !== ''){
                $input['SourceURL'] = $request->SourceURL;
            }
            if($request->has('IsPublished') && $request->IsPublished !== ''){
                $input['IsPublished'] = $request->IsPublished;
            }
            if($request->has('keyword') && $request->keyword !== ''){
                $input['keyword'] = $request->keyword;
            }
            if($request->has('meta_description') && $request->meta_description !== ''){
                $input['meta_description'] = $request->meta_description;
            }
            $content = ContentModel::create($input);
            if ($content) {
                if($request->hasFile('upload_file')){
                    $image = $request->file('upload_file')->getClientOriginalName();
                    $ext = $request->file('upload_file')->getClientOriginalExtension();
                    $image = 'images/contents/'.Carbon::now()->timestamp.'_'.str_replace(' ','-',$image);
                    Storage::disk('public')->put($image, file_get_contents($request->file('upload_file')->getRealPath()));

                    $articleMedia = new ContentMediaModel();
                    $articleMedia->URL = $image;
					$articleMedia->SortNo = 1;
                    $articleMedia->IDContent = $content->ID;
                    $articleMedia->IsPrimary = 1;
					$articleMedia->AddedTime = time();
					$articleMedia->AddedByIP = $request->ip();
					$articleMedia->EditedTime = time();
					$articleMedia->EditedByIP = $request->ip();
                    $articleMedia->Type = 'Image';
                    $articleMedia->save();
                }
                
                $redirect = redirect()->route('admin.content.index');
                back()->with('success',trans('message.create.success'));
                \DB::commit();
                return $redirect;
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            dd($message);
            \DB::rollback();
            back()->with('error',trans('message.create.error') . ' : ' . $e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function edit($id)
    {
        $title = $this->title;
        $listdata = ContentModel::find($id);
        $listContents = ContentModel::pluck('Title','Title')->all();
        return view('admin.content.edit',compact('listdata','title','listContents'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'Title' => 'required|unique:contents,Title,'.$id
        ]);

        \DB::beginTransaction();
        try {

            $update = ContentModel::find($id);
            $input = ['Title' => $request->input('Title')];
            if($request->has('ShortContent') && $request->ShortContent !== ''){
                $input['ShortContent'] = strip_tags($request->ShortContent);
            }
            if($request->has('Content') && $request->Content !== ''){
                $input['Content'] = $request->Content;
            }
            if($request->has('IDPrevArticle') && $request->IDPrevArticle !== ''){
                $input['IDPrevArticle'] = $request->IDPrevArticle;
            }
            if($request->has('IDNextArticle') && $request->IDNextArticle !== ''){
                $input['IDNextArticle'] = $request->IDNextArticle;
            }
            if($request->has('SourceURL') && $request->SourceURL !== ''){
                $input['SourceURL'] = $request->SourceURL;
            }
            if($request->has('IsPublished') && $request->IsPublished !== ''){
                $input['IsPublished'] = $request->IsPublished;
            }
            if($request->has('keyword') && $request->keyword !== ''){
                $input['keyword'] = $request->keyword;
            }
            if($request->has('meta_description') && $request->meta_description !== ''){
                $input['meta_description'] = $request->meta_description;
            }
            $update->update($input);

            if ($update) {

                if($request->hasFile('upload_file')){
                    $articleMedia = ContentMediaModel::where('IDContent',$id)->first();
                    $image = $request->file('upload_file')->getClientOriginalName();
                    $ext = $request->file('upload_file')->getClientOriginalExtension();
                    $image = 'images/contents/'.Carbon::now()->timestamp.'_'.str_replace(' ','-',$image);
                    Storage::disk('public')->put($image, file_get_contents($request->file('upload_file')->getRealPath()));
                    if(empty($articleMedia)){
                        $articleMedia = new ContentMediaModel();
                        $articleMedia->URL = $image;
                        $articleMedia->SortNo = 1;
                        $articleMedia->IDContent = $id;
                        $articleMedia->IsPrimary = 1;
                        $articleMedia->AddedTime = time();
                        $articleMedia->AddedByIP = $request->ip();
                        $articleMedia->EditedTime = time();
                        $articleMedia->EditedByIP = $request->ip();
                        $articleMedia->Type = 'Image';
                        $articleMedia->save();
                    }else{
                        if($articleMedia->URL !="" && Storage::disk('public')->exists($articleMedia->URL)){
                            Storage::disk('public')->delete($articleMedia->URL);
                        }
                        $articleMedia->URL = $image;
                        $articleMedia->AddedByIP = $request->ip();
                        $articleMedia->EditedTime = time();
                        $articleMedia->EditedByIP = $request->ip();
                        $articleMedia->Type = 'Image';
                        $articleMedia->save();
                    }
                }

                back()->with('success',trans('message.update.success'));
                \DB::commit();
                return redirect()->route( 'admin.content.index' );;
            }
        } catch (\Exception $e) {
            \DB::rollback();
            back()->with('error',trans('message.update.error') . ' : ' . $e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function destroy($id, Request $request)
    {
        $data = ContentModel::findOrFail($id);
        $data->delete();
        back()->with('success',trans('message.delete.success'));
    }

    private function validator(Request $request)
    {
        $rules = [
            'Title' => 'required|unique:contents,Title',
            // 'permission' => 'required',
        ];

        $messages = [];

        $request->validate($rules,$messages);
    }
}
