<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use DataTables;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->title = 'Permissions';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = $this->title;

        if($request->ajax())
        {
            $data = Permission::orderBy('id','DESC')->get();
            
            return DataTables::of($data)
                ->addColumn('action', function($data){
                    $button = '<div class="btn-group m-t-15">';
                        $button .= '<button type="button" class="btn btn-sm btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Action</button>';
                        $button .='<div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">';
                            $button .= '<a class="dropdown-item" href="'.route('admin.permission.edit', $data->id).'"><i class="fa fa-pencil"></i> Edit</a>';
                            $button .= '<a class="dropdown-item delete" href="#" id="'.$data->id.'"><i class="fa fa-trash"></i> Delete</a>';
                        $button .= '</div>';
                    $button .= '</div>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.permission.index', compact('title'));
    }

    public function create()
    {
        $title = $this->title;
        return view('admin.permission.create', compact('title','permission'));
    }

    public function store(Request $request)
    {
        $this->validator($request);
        
        \DB::beginTransaction();
        try {

            $permission = Permission::create(['name' => $request->input('name')]);

            if ($permission) {
                $redirect = redirect()->route('admin.permission.index');
                back()->with('success',trans('message.create.success'));
                \DB::commit();
                return $redirect;
            }
        } catch (\Exception $e) {
            \DB::rollback();
            back()->with('error',trans('message.create.error') . ' : ' . $e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function edit($id)
    {
        $title = $this->title;
        $listdata = Permission::find($id);
        return view('admin.permission.edit',compact('listdata','title'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:permissions,name,'.$id
        ]);

        \DB::beginTransaction();
        try {

            $update = Permission::find($id);
            $update->update(['name' => $request->input('name')]);

            if ($update) {
                back()->with('success',trans('message.update.success'));
                \DB::commit();
                return redirect()->route( 'admin.permission.index' );;
            }
        } catch (\Exception $e) {
            \DB::rollback();
            back()->with('error',trans('message.update.error') . ' : ' . $e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function destroy($id, Request $request)
    {
        DB::table("permissions")->where('id',$id)->delete();
        back()->with('success',trans('message.delete.success'));
    }

    private function validator(Request $request)
    {
        $rules = [
            'name' => 'required|unique:permissions,name',
            // 'permission' => 'required',
        ];

        $messages = [];

        $request->validate($rules,$messages);
    }
}
