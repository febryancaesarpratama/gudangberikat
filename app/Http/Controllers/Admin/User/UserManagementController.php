<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use App\User;

use DB;
use DataTables;
use Notification;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Lang;

class UserManagementController extends Controller
{

    public function __construct()
    {
        $this->title = 'User';

        // $this->middleware('permission:user-list');
        // $this->middleware('permission:user-create', ['only' => ['create','store']]);
        // $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        // $this->middleware('permission:user-delete', ['only' => ['destroy']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = $this->title;
        $roles = Role::pluck('name','id')->all();
        if($request->ajax())
        {
            $data = User::query()
            ->select('id','email','is_active','first_name','last_name','email_verified_at','login_name')
            ->leftJoin('model_has_roles','model_has_roles.model_id','=','users.id')
            ->where('users.is_superadmin',0);
            if($request->has('searchByDateStart') && $request->searchByDateStart !='-1' && $request->has('searchByDateEnd') && $request->searchByDateEnd !='-1'){
                $date_start = $request->searchByDateStart;
                $date_end = $request->searchByDateEnd;
                $data->whereBetween('users.email_verified_at',[$date_start,$date_end]);
            }
            if($request->has('role') && $request->role !='-1' && !empty($request->role)){
                $role = $request->role;
                $data->where('model_has_roles.role_id',$role);
            }
            if($request->has('status') && $request->status !='-1'){
                $status = $request->status;
                $data->where('users.is_active',$status);
            }
           
            $data = $data->latest()->groupBy('users.id')->get();
            foreach ($data as $key => $value) {
                $data[$key]->role = $value->getRoleNames();
            }
            return DataTables::of($data)
                ->addColumn('activated', function($data){
                    $html = '';
                    if($data->is_active == 1){
                        $html .= '<span class="badge badge-success">Active</span>';
                    }else{
                        $html .= '<span class="badge badge-danger">Inactive</span>';
                    }
                    return $html;
                })
                ->addColumn('full_name', function($data){
                    $html = "";
                    $html .= $data->first_name.' '.$data->last_name;

                    return $html;
                })
                ->addColumn('verified_at', function($data){
                    $html = "-";
                    if(!empty($data->email_verified_at)){
                        $html = $data->email_verified_at;
                    }
                    return $html;
                })
                ->addColumn('action', function($data){
                    $button = '<div class="btn-group m-t-15">';
                        $button .= '<button type="button" class="btn btn-sm btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Action</button>';
                        $button .='<div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">';
                            $button .= '<a class="dropdown-item" href="'.route('admin.user.edit', $data->id).'"><i class="fa fa-pencil"></i> Edit</a>';
                            $button .= '<a class="dropdown-item delete" href="#" id="'.$data->id.'"><i class="fa fa-trash"></i> Delete</a>';
                            $button .= '<a class="dropdown-item" href="#"><i class="fa fa-check"></i> activated</a>';
                        $button .= '</div>';
                    $button .= '</div>';
                    return $button;
                })
                ->rawColumns(['action','activated','verified_at','full_name'])
                ->make(true);
        }
        return view('admin.user.index', compact('title','roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = $this->title;
        $roles = Role::pluck('name','name')->all();
        return view('admin.user.create', compact('title','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request);
        
        \DB::beginTransaction();
        try {

            $insert = User::create($request->all());

            if ($insert) {

                $roles = $request->input('roles') ? $request->input('roles') : [];
                $insert->assignRole($roles);
                \DB::commit();
                $tokenstring = $this->getToken();
                $userdata = User::find($insert->id);
                $userdata->remember_token = $tokenstring;
                $userdata->update();

                $redirect = redirect()->route('admin.user.index');
                back()->with('success',trans('message.create.success'));
                
                return $redirect;
            }
        } catch (\Exception $e) {
            \DB::rollback();
            back()->with('error',trans('message.create.error') . ' : ' . $e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = $this->title;
        $listdata = User::find($id);
        $roles = Role::pluck('name','name')->all();
        $userRole = $listdata->roles->pluck('name','name')->all();
        $finds = array();
        return view('admin.user.edit',compact('listdata','roles','title','userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'login_name' => 'required|unique:users,login_name,'.$id.'|min:6|regex:/^[a-zA-Z0-9-_@.+]+$/',
            'email' => 'required|email|unique:users,email,'.$id,
            'first_name' => 'required|string',
            'password' => 'sometimes|nullable|min:6',
        ]);

        \DB::beginTransaction();
        try {
            $input = $request->all();
            $user = User::find($id);
            
            $user->update($input);
            DB::table('model_has_roles')->where('model_id',$id)->delete();
            $user->assignRole($request->input('roles'));

            back()->with('success',trans('message.update.success'));
            \DB::commit();
            return redirect()->route( 'admin.user.index' );
        } catch (Exception $e) {
            \DB::rollback();
            back()->with('error',trans('message.update.error') . ' : ' . $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::findOrFail($id);
        $data->delete();
        back()->with('success',trans('message.delete.success'));
    }

    private function validator(Request $request)
    {
        $rules = [
            'login_name' => 'required|unique:users|min:6|regex:/^[a-zA-Z0-9-_@.+]+$/',
            'email' => 'required|email|unique:users',
            'first_name' => 'required|string',
            'password' => 'required|string|min:6',
        ];

        $messages = [
            'login_name.unique' => 'Login Name is already registered',
            'email.unique' => 'That email address is already registered.',
            'login_name.regex' => 'That login name must contain Alphanumeric, dash and underscore.',
        ];

        $request->validate($rules,$messages);
    }
}
