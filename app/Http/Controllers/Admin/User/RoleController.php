<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use DataTables;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->title = 'Roles';
    }

    public function index(Request $request){
        $title = $this->title;

        if($request->ajax())
        {
            $data = Role::orderBy('id','DESC')->get();
            
            return DataTables::of($data)
                ->addColumn('action', function($data){
                    $button = '<div class="btn-group m-t-15">';
                        $button .= '<button type="button" class="btn btn-sm btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Action</button>';
                        $button .='<div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">';
                            $button .= '<a class="dropdown-item" href="'.route('admin.role.edit', $data->id).'"><i class="fa fa-pencil"></i> Edit</a>';
                            $button .= '<a class="dropdown-item delete" href="#" id="'.$data->id.'"><i class="fa fa-trash"></i> Delete</a>';
                        $button .= '</div>';
                    $button .= '</div>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.role.index', compact('title'));
    }

    public function create()
    {
        $title = $this->title;
        $permission = Permission::get()->sortBy('name');
        return view('admin.role.create', compact('title','permission'));
    }

    public function store(Request $request)
    {
        $this->validator($request);
        
        \DB::beginTransaction();
        try {
            
            $role = Role::create(['name' => $request->input('name')]);
            $role->syncPermissions($request->input('permission'));

            if ($role) {
                $redirect = redirect()->route('admin.role.index');
                back()->with('success',trans('message.create.success'));
                \DB::commit();
                return $redirect;
            }
        } catch (\Exception $e) {
            \DB::rollback();
            back()->with('error',trans('message.create.error') . ' : ' . $e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function edit($id)
    {
        $title = $this->title;
        $listdata = Role::find($id);
        $permission = Permission::get()->sortBy('name');
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();


        return view('admin.role.edit',compact('listdata','permission','rolePermissions','title'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name,'.$id,
            'permission' => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $update = Role::find($id);
            $update->name = $request->input('name');
            $update->save();

            $update->syncPermissions($request->input('permission'));

            if ($update) {
                back()->with('success',trans('message.update.success'));
                \DB::commit();
                return redirect()->route( 'admin.role.index' );;
            }
        } catch (\Exception $e) {
            \DB::rollback();
            back()->with('error',trans('message.update.error') . ' : ' . $e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function destroy($id, Request $request)
    {
        DB::table("roles")->where('id',$id)->delete();
        back()->with('success',trans('message.delete.success'));
    }

    private function validator(Request $request)
    {
        $rules = [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ];

        $messages = [];

        $request->validate($rules,$messages);
    }

}
