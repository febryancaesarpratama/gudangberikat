<?php

namespace App\Http\Controllers\Admin\Auth;

use Auth;
use App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins; 
use Illuminate\Support\Facades\Cache;
use DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating admin users for the application and
    | redirecting them to your admin dashboard.
    |
    */

    /**
     * This trait has all the login throttling functionality.
     */
    use ThrottlesLogins;

    /**
     * Max login attempts allowed.
     */
    public $maxAttempts = 3;

    /**
     * Number of minutes to lock the login.
     */
    public $decayMinutes = 10;

    /**
     * Only guests for "admin" guard are allowed except
     * for logout.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
        $this->username = $this->findLoginname();
    }

    /**
     * Show the login form.
     * 
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login',[
            'title' => 'Admin Login',
            'loginRoute' => 'admin.login',
            'forgotPasswordRoute' => 'admin.password.request',
        ]);
    }

    /**
     * Login the admin.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(Request $request)
    {
        $this->validator($request);
        
        //check if the user has too many login attempts.
        if ($this->hasTooManyLoginAttempts($request)){
            //Fire the lockout event
            $this->fireLockoutEvent($request);

            //redirect the user back after lockout.
            return $this->sendLockoutResponse($request);
        }

        // $datarole = $this->findUserrole($request);

        // if(!$datarole){
        //     Auth::guard('admin')->logout();
        //     Cache::flush();
        //     return redirect('/');
        // }
        
        //attempt login.
        $request['is_active'] = 1;
        if(Auth::guard('admin')->attempt($request->only($this->username,'password','is_active'))){
            //Authenticated, redirect to the intended route
            //if available else admin dashboard.
            
            $locale = 'en';
            if(Auth::guard('admin')->user()->language != ''){
                $locale = Auth::guard('admin')->user()->language;
            }
            App::setLocale($locale);
            session()->put('locale', $locale);

            return redirect()
                ->intended(route('admin.user.index'))
                ->with('status','You are Logged in as Admin!');
        }

        //keep track of login attempts from the user.
        $this->incrementLoginAttempts($request);

        //Authentication failed, redirect back with input.
        return $this->loginFailed();
    }

    /**
     * Logout the admin.
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        Auth::guard('admin')->logout();
        Cache::flush();
        return redirect()
            ->route('admin.login')
            ->with('status','Admin has been logged out!');
    }

    /**
     * Validate the form data.
     * 
     * @param \Illuminate\Http\Request $request
     * @return 
     */
    private function validator(Request $request)
    {
        //validation rules.
        $rules = [
            $this->username() => 'string|exists:users',
            'password' => 'required|string',
        ];

        //custom validation error messages.
        $messages = [
            'login_name.exists' => 'Login Name do not match our records',
            'email.exists' => 'These credentials do not match our records.',
            'password.required' => 'Password cannot be empty',
        ];

        //validate the request.
        $request->validate($rules,$messages);
    }

    /**
     * Redirect back after a failed login.
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    private function loginFailed(){
        return redirect()
            ->back()
            ->withInput()
            ->with('error','Login failed, please try again!');
    }

    /**
     * Username used in ThrottlesLogins trait
     * 
     * @return string
     */
    public function username(){
        return $this->username;
    }

    public function findLoginname(){
        $login = request()->input('email');
        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'login_name';
        request()->merge([$fieldType => $login]);
        return $fieldType;
    }

    public function findUserrole($request){
        $result = true;
        $ceklogin =  Auth::guard('admin')->attempt($request->only($this->username,'password','is_active'));
        if($ceklogin){
            $roleuser = Auth::guard('admin')->user()->getRoleNames();
            if(count($roleuser) == 1){
                foreach ($roleuser as $key => $value) {
                    if($value == 'Learner'){
                        $result = false;
                    }
                }
            }
        }
        return $result;
    }
}
