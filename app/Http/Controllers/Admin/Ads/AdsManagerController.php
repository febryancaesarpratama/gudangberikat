<?php

namespace App\Http\Controllers\Admin\Ads;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\AdsManagerModel;
use App\Models\AdsMediaModel;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

use DB;
use DataTables;
use Carbon\Carbon;

class AdsManagerController extends Controller
{
    public function __construct()
    {
        $this->title = 'Content';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = $this->title;

        if($request->ajax())
        {
            $data = AdsManagerModel::orderBy('id','DESC')->get();
            
            return DataTables::of($data)
                ->addColumn('action', function($data){
                    $button = '<div class="btn-group m-t-15">';
                        $button .= '<button type="button" class="btn btn-sm btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Action</button>';
                        $button .='<div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">';
                            $button .= '<a class="dropdown-item" href="'.route('admin.adsmanager.edit', $data->id).'"><i class="fa fa-pencil"></i> Edit</a>';
                            $button .= '<a class="dropdown-item delete" href="#" id="'.$data->id.'"><i class="fa fa-trash"></i> Delete</a>';
                        $button .= '</div>';
                    $button .= '</div>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.ads.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = $this->title;
        $list_ukuran = [
            'col-md-1' => '12 kolom 1 baris',
            'col-md-2' => '6 kolom 1 baris',
            'col-md-3' => '4 kolom 1 baris',
            'col-md-4' => '3 kolom 1 baris',
            'col-md-5' => '2+1 kolom 1 baris',
            'col-md-6' => '2 kolom 1 baris',
            'col-md-7' => '1 kolom + 2 kolom kecil 1 baris',
            'col-md-8' => '1 kolom + 2 kolom kecil 1 baris',
            'col-md-9' => '1 kolom + 2 kolom kecil 1 baris',
            'col-md-10' => '1 kolom + 1 kolom kecil 1 baris',
            'col-md-11' => '1 kolom + 1 kolom kecil 1 baris',
            'col-md-12' => '1 kolom 1 baris',
        ];
        return view('admin.ads.create', compact('title','list_ukuran'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request);
        
        \DB::beginTransaction();
        try {
            $input = ['Title' => $request->input('Title'),'Content' => $request->input('Content'),
            'IsPrimary' => $request->input('IsPrimary'),'StartPeriod' => $request->input('StartPeriod'),
            'EndPeriod' => $request->input('EndPeriod'),'Name' => $request->input('Name'),
            'Email' => $request->input('Email'),'PhoneNo' => $request->input('PhoneNo'),'AdsFee' => $request->input('AdsFee'),
            'IsVerified' => $request->input('IsVerified')];

            $input['StartPeriod'] = Carbon::parse($request->input('StartPeriod'))->format('Y-m-d');
            $input['EndPeriod'] = Carbon::parse($request->input('EndPeriod'))->format('Y-m-d');

            if($request->has('ShortContent') && $request->ShortContent !== ''){
                $input['ShortContent'] = strip_tags($request->ShortContent);
            }
            if($request->has('Location') && $request->Location !== ''){
                $input['Location'] = $request->Location;
            }
            if($request->has('DestinationURL') && $request->DestinationURL !== ''){
                $input['DestinationURL'] = $request->DestinationURL;
            }
            if($request->has('IDContent') && $request->IDContent !== ''){
                $input['IDContent'] = $request->IDContent;
            }
            if($request->has('Currency') && $request->Currency !== ''){
                $input['Currency'] = $request->Currency;
            }
            if($request->has('CurrencySign') && $request->CurrencySign !== ''){
                $input['CurrencySign'] = $request->CurrencySign;
            }
            if($request->has('boostrap_class') && $request->boostrap_class !== ''){
                $input['boostrap_class'] = $request->boostrap_class;
            }
            $content = AdsManagerModel::create($input);
            if ($content) {
                if($request->hasFile('upload_file')){
                    $image = $request->file('upload_file')->getClientOriginalName();
                    $ext = $request->file('upload_file')->getClientOriginalExtension();
                    $image = 'images/adsmedia/'.Carbon::now()->timestamp.'_'.str_replace(' ','-',$image);
                    Storage::disk('public')->put($image, file_get_contents($request->file('upload_file')->getRealPath()));

                    $articleMedia = new AdsMediaModel();
                    $articleMedia->Location = $image;
                    $articleMedia->IsCover = 'y';
					$articleMedia->SortNo = 1;
                    $articleMedia->id_media = $content->id;
                    $articleMedia->IsPrimary = 'y';
                    $articleMedia->save();
                }
                
                $redirect = redirect()->route('admin.adsmanager.index');
                back()->with('success',trans('message.create.success'));
                \DB::commit();
                return $redirect;
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            dd($message);
            \DB::rollback();
            back()->with('error',trans('message.create.error') . ' : ' . $e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = $this->title;
        $startPeriod = '00-00-0000';
        $endPeriod = '00-00-0000';
        $listdata = AdsManagerModel::find($id);
        if(!empty($listdata)){
            $startPeriod = Carbon::parse($listdata->StartPeriod)->format('Y-m-d');
            $endPeriod = Carbon::parse($listdata->EndPeriod)->format('Y-m-d');
        }
        $list_ukuran = [
            'col-md-1' => '12 kolom 1 baris',
            'col-md-2' => '6 kolom 1 baris',
            'col-md-3' => '4 kolom 1 baris',
            'col-md-4' => '3 kolom 1 baris',
            'col-md-5' => '2+1 kolom 1 baris',
            'col-md-6' => '2 kolom 1 baris',
            'col-md-7' => '1 kolom + 2 kolom kecil 1 baris',
            'col-md-8' => '1 kolom + 2 kolom kecil 1 baris',
            'col-md-9' => '1 kolom + 2 kolom kecil 1 baris',
            'col-md-10' => '1 kolom + 1 kolom kecil 1 baris',
            'col-md-11' => '1 kolom + 1 kolom kecil 1 baris',
            'col-md-12' => '1 kolom 1 baris',
        ];
        return view('admin.ads.edit',compact('listdata','title','startPeriod','endPeriod','list_ukuran'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'Title' => 'required',
            'Content' => 'required',
            'IsPrimary' => 'required',
            'StartPeriod' => 'required',
            'EndPeriod' => 'required',
            'Name' => 'required',
            'Email' => 'required',
            'PhoneNo' => 'required',
            'AdsFee' => 'required',
            'IsVerified' => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $update = AdsManagerModel::find($id);
            $input = ['Title' => $request->input('Title'),'Content' => $request->input('Content'),
            'IsPrimary' => $request->input('IsPrimary'),'StartPeriod' => $request->input('StartPeriod'),
            'EndPeriod' => $request->input('EndPeriod'),'Name' => $request->input('Name'),
            'Email' => $request->input('Email'),'PhoneNo' => $request->input('PhoneNo'),'AdsFee' => $request->input('AdsFee'),
            'IsVerified' => $request->input('IsVerified')];

            $input['StartPeriod'] = Carbon::parse($request->input('StartPeriod'))->format('Y-m-d');
            $input['EndPeriod'] = Carbon::parse($request->input('EndPeriod'))->format('Y-m-d');

            if($request->has('ShortContent') && $request->ShortContent !== ''){
                $input['ShortContent'] = strip_tags($request->ShortContent);
            }
            if($request->has('Location') && $request->Location !== ''){
                $input['Location'] = $request->Location;
            }
            if($request->has('DestinationURL') && $request->DestinationURL !== ''){
                $input['DestinationURL'] = $request->DestinationURL;
            }
            if($request->has('IDContent') && $request->IDContent !== ''){
                $input['IDContent'] = $request->IDContent;
            }
            if($request->has('Currency') && $request->Currency !== ''){
                $input['Currency'] = $request->Currency;
            }
            if($request->has('CurrencySign') && $request->CurrencySign !== ''){
                $input['CurrencySign'] = $request->CurrencySign;
            }
            if($request->has('boostrap_class') && $request->boostrap_class !== ''){
                $input['boostrap_class'] = $request->boostrap_class;
            }
            
            $update->update($input);

            if ($update) {

                if($request->hasFile('upload_file')){
                    $articleMedia = AdsMediaModel::where('IDContent',$id)->first();
                    $image = $request->file('upload_file')->getClientOriginalName();
                    $ext = $request->file('upload_file')->getClientOriginalExtension();
                    $image = 'images/adsmedia/'.Carbon::now()->timestamp.'_'.str_replace(' ','-',$image);
                    Storage::disk('public')->put($image, file_get_contents($request->file('upload_file')->getRealPath()));
                    if(empty($articleMedia)){
                        $articleMedia = new AdsMediaModel();
                        $articleMedia->Location = $image;
                        $articleMedia->IsCover = 'y';
                        $articleMedia->SortNo = 1;
                        $articleMedia->id_media = $update->id;
                        $articleMedia->IsPrimary = 'y';
                        $articleMedia->save();
                    }else{
                        if($articleMedia->Location !="" && Storage::disk('public')->exists($articleMedia->Location)){
                            Storage::disk('public')->delete($articleMedia->Location);
                        }
                        $articleMedia->Location = $image;
                        $articleMedia->IsCover = 'y';
                        $articleMedia->SortNo = 1;
                        $articleMedia->id_media = $update->id;
                        $articleMedia->IsPrimary = 'y';
                        $articleMedia->save();
                    }
                }

                back()->with('success',trans('message.update.success'));
                \DB::commit();
                return redirect()->route( 'admin.adsmanager.index' );;
            }
        } catch (\Exception $e) {
            \DB::rollback();
            back()->with('error',trans('message.update.error') . ' : ' . $e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = AdsManagerModel::findOrFail($id);
        $data->delete();
        back()->with('success',trans('message.delete.success'));
    }

    private function validator(Request $request)
    {
        $rules = [
            'Title' => 'required',
            'Content' => 'required',
            'IsPrimary' => 'required',
            'StartPeriod' => 'required',
            'EndPeriod' => 'required',
            'Name' => 'required',
            'Email' => 'required',
            'PhoneNo' => 'required',
            'AdsFee' => 'required',
            'IsVerified' => 'required',
        ];

        $messages = [];

        $request->validate($rules,$messages);
    }
}
