<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JadwalSholatModel;
use App\Models\MasterKota;
use Carbon\Carbon;
use Log;

class JadwalSholatController extends Controller
{
    public function getJadwalSholat(Request $request){
        
        $file_path = 'public/json/kalender.json';
        $kalender = file_get_contents(base_path($file_path));
        $kalender = json_decode($kalender,true);
        
        $file_path = 'public/json/daftar_kota_mini.json';
        $daftar_kota_json = file_get_contents(base_path($file_path));
        $daftar_kota_json = json_decode($daftar_kota_json,true);
        $no_progress = 1;
        foreach ($kalender['kalender'] as $k => $v) {
            if(count($v['daftar_kota']) == 0 && $no_progress == 1){
                $today = $v['date'];
                if(count($daftar_kota_json['kota']) > 0){
                    foreach ($daftar_kota_json['kota'] as $kota) {
                        $url_api = "http://api.banghasan.com/sholat/format/json/jadwal/kota/".$kota['id']."/tanggal/".$today;
                        $response_api = $this->getDataCurl($url_api,array(),'');
                        $kota_id = 0;
                        $check_kota = MasterKota::where('kota','like',"%".$kota['nama']."%")->first();
                        if(!empty($check_kota)){
                            $kota_id = $check_kota->id;
                        }
                        $jadwal_sholat_hari_ini[] = array(
                            'id_kota'       => $kota['id'],
                            'kota'          => $kota['nama'],
                            'kota_internal' => $kota_id,
                            'jadwal_sholat' => $response_api['jadwal']['data']
                        );

                        // input tabel jadwal sholat
                        $input = array(
                            'kota_id'           => $kota_id,
                            'kota_external_id'  => $kota['id'],
                            'tanggal'           => $today,
                            'lat'               => '-',
                            'lng'               => '-',
                            'country_id'        => 1,
                            'imsak'             => $response_api['jadwal']['data']['imsak'],
                            'subuh'             => $response_api['jadwal']['data']['subuh'],
                            'terbit'            => $response_api['jadwal']['data']['terbit'],
                            'dhuha'             => $response_api['jadwal']['data']['dhuha'],
                            'dzuhur'            => $response_api['jadwal']['data']['dzuhur'],
                            'ashar'             => $response_api['jadwal']['data']['ashar'],
                            'maghrib'           => $response_api['jadwal']['data']['maghrib'],
                            'isya'              => $response_api['jadwal']['data']['isya'],
                        );
                        $insert = JadwalSholatModel::create($input);
                        // input tabel jadwal sholat
                    }
                    $kalender['kalender'][$k]['daftar_kota'] = $jadwal_sholat_hari_ini;
                    $file_path = 'public/json/kalender.json';
                    $file_content = json_encode($kalender);
                    file_put_contents(base_path($file_path),$file_content);
                }
                $no_progress++;
            }
        }
        dd($jadwal_sholat_hari_ini);
    }


    public function getDataCurl($url,$filter,$token =''){
        $header = [
            "headers" => []
        ];

        try {
            $client = new \GuzzleHttp\Client($header);
            $url_destination = $url;
            $response = $client->get($url_destination,['query'=>$filter]);
            $status = $response->getStatusCode();
            Log::warning('curl data', ['header' => $header,'url'=>$url,'response'=>$status,'filter'=>$filter,'token'=>$token]);
            
            if($status == 200){
                if(!empty($response)){
                   
                    $response = json_decode($response->getBody(),true);
                    return $response;
                }
            }
        }catch(\Guzzle\Http\Exception\ConnectException $e) {
            $return = array($this->STATUS=>false,$this->CODE=>500,$this->MESSAGE=>'Terjadi Gangguan Koneksi '.$e->getMessage(),$this->RESULT=>'');
            Log::warning('curl data eror 1', ['header' => $header,'url'=>$url,'response'=>$e->getMessage(),'filter'=>$filter,'token'=>$token]);
            
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $return = array($this->STATUS=>false,$this->CODE=>501,$this->MESSAGE=>'Gagal Melakukan Request ke service '.$e->getMessage(),$this->RESULT=>'');
            Log::warning('curl data erro 2', ['header' => $header,'url'=>$url,'response'=>$e->getMessage(),'filter'=>$filter,'token'=>$token]);
        }

        return $return;
    }
}
