<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use App\Models\PageModel;
use App\Models\PageMenuModel;
use App\Models\ContentModel;
use App\Models\JadwalSholatModel;
use App\Models\AdsManagerModel;
use App\Services\GenerateArtikel;
use Carbon\Carbon;


class DynamisController extends Controller
{

    protected $generateArtikel;

    public function __construct(GenerateArtikel $generateArtikel)
    {
        $this->generateArtikel = $generateArtikel;
    }

    public function index(Request $request){
        $url = $request->segment(1);
        if(empty($url)){
            $url = (env('DEFAULT_PAGE')!="")?'/'.env('DEFAULT_PAGE'):'/home';
        }else{
            $url = '/'.$url;
        }
        $data_page = PageModel::where('URL',$url)->first();
        $default_menus = PageMenuModel::where('is_default',1)->get();

        $file_path = 'public/json/daftar_kota_mini.json';
        $daftar_kota_json = file_get_contents(base_path($file_path));
        $daftar_kota_json = json_decode($daftar_kota_json,true);
        foreach ($daftar_kota_json['kota'] as $kota) {
            $daftar_kota[] = array('id'=>$kota['id'],'nama'=>$kota['nama'],'slug'=>Str::slug($kota['nama'], '-'));
        }
        $ads_datas = [];
        $jadwal_sholat = array();
        $jadwal_sholat_satu_bulan = array();
        $filter_tanggal = Carbon::now()->format('Y-m-d');
        $filter_kota = 667;
        if($request->has('filter_kota') && $request->filter_kota > 0){
            $filter_kota = $request->filter_kota;
        }
        if($request->has('filter_tanggal') && $request->filter_tanggal > 0){
            $filter_tanggal = Carbon::parse($request->filter_tanggal)->format('Y-m-d');
        }

        if(env('WIDGET_JADWAL_SHOLAT','off') == 'on'){
            $jadwal_sholat = JadwalSholatModel::where('tanggal',$filter_tanggal)->where('kota_external_id',$filter_kota)->first();
            if(!empty($jadwal_sholat)){
                $jadwal_sholat = $jadwal_sholat->toArray();
            }
            $jadwal_sholat_satu_bulan = JadwalSholatModel::whereRaw("date_format(tanggal,'%Y-%m') = '".Carbon::now()->format('Y-m')."'")->where('kota_external_id',$filter_kota)->get();
            if(!empty($jadwal_sholat_satu_bulan)){
                $jadwal_sholat_satu_bulan = $jadwal_sholat_satu_bulan->toArray();
            }
        }
        $bulan = Carbon::now()->format('M');
        $ads = AdsManagerModel::query()
        ->select('id','Title','DestinationURL','ShortContent','Content')
        ->whereRaw("DATE_FORMAT( StartPeriod, '%Y-%m-%d' ) <= now() AND DATE_FORMAT( EndPeriod, '%Y-%m-%d' ) >= now()
        AND IsVerified = 'y'")
        ->orderBy('AdsFee','desc');
        $ads = $ads->get();
        if($ads->count() > 0){
            foreach($ads as $v){
                $ads_datas[] = array(
                    'id'    => $v->id,
                    'title' => $v->Title,
                    'destination_url' => $v->DestinationURL,
                    'short_content'   => $v->short_content,
                    'content' => $v->Content,
                    'image' => $v->media->Location,
                );
            }
        }

        // return $ads_datas;

        return view('front', compact('url','data_page','default_menus','jadwal_sholat','jadwal_sholat_satu_bulan','daftar_kota','filter_kota','bulan','ads','ads_datas'));
    }

    public function index_kota(Request $request,$kota_id){
        $url = $request->segment(1);
        if(empty($url)){
            $url = (env('DEFAULT_PAGE')!="")?'/'.env('DEFAULT_PAGE'):'/home';
        }else{
            $url = '/'.$url;
        }
        $data_page = PageModel::where('URL',$url)->first();
        $default_menus = PageMenuModel::where('is_default',1)->get();

        $jadwal_sholat = array();
        $jadwal_sholat_satu_bulan = array();
        $filter_tanggal = Carbon::now()->format('Y-m-d');
        $file_path = 'public/json/daftar_kota_mini.json';
        $daftar_kota_json = file_get_contents(base_path($file_path));
        $daftar_kota_json = json_decode($daftar_kota_json,true);
        $filter_kota = $kota_id;
        foreach ($daftar_kota_json['kota'] as $kota) {
            $daftar_kota[] = array('id'=>$kota['id'],'nama'=>$kota['nama'],'slug'=>Str::slug($kota['nama'], '-'));
            if(!empty($kota_id) && Str::slug($kota['nama'], '-') == $kota_id){
                $filter_kota = $kota['id'];
            }
        }
        
        if($request->has('filter_kota') && $request->filter_kota > 0){
            $filter_kota = $request->filter_kota;
        }
        if($request->has('filter_tanggal') && $request->filter_tanggal > 0){
            $filter_tanggal = Carbon::parse($request->filter_tanggal)->format('Y-m-d');
        }

        if(env('WIDGET_JADWAL_SHOLAT','off') == 'on'){
            $jadwal_sholat = JadwalSholatModel::where('tanggal',$filter_tanggal)->where('kota_external_id',$filter_kota)->first();
            if(!empty($jadwal_sholat)){
                $jadwal_sholat = $jadwal_sholat->toArray();
            }
            $jadwal_sholat_satu_bulan = JadwalSholatModel::whereRaw("date_format(tanggal,'%Y-%m') = '".Carbon::now()->format('Y-m')."'")->where('kota_external_id',$filter_kota)->get();
            if(!empty($jadwal_sholat_satu_bulan)){
                $jadwal_sholat_satu_bulan = $jadwal_sholat_satu_bulan->toArray();
            }
        }
        $bulan = Carbon::now()->format('M');
        return view('front', compact('url','data_page','default_menus','jadwal_sholat','jadwal_sholat_satu_bulan','daftar_kota','filter_kota','bulan'));
    }

    public function index_kota_tanggal(Request $request,$kota_id,$tanggal){
        $url = $request->segment(1);
        if(empty($url)){
            $url = (env('DEFAULT_PAGE')!="")?'/'.env('DEFAULT_PAGE'):'/home';
        }else{
            $url = '/'.$url;
        }
        $data_page = PageModel::where('URL',$url)->first();
        $default_menus = PageMenuModel::where('is_default',1)->get();

        $jadwal_sholat = array();
        $jadwal_sholat_satu_bulan = array();
        $filter_tanggal = Carbon::parse($tanggal)->format('Y-m-d');
        $filter_tanggal_bulan = Carbon::parse($tanggal)->format('Y-m');

        $file_path = 'public/json/daftar_kota_mini.json';
        $daftar_kota_json = file_get_contents(base_path($file_path));
        $daftar_kota_json = json_decode($daftar_kota_json,true);
        $filter_kota = $kota_id;
        foreach ($daftar_kota_json['kota'] as $kota) {
            $daftar_kota[] = array('id'=>$kota['id'],'nama'=>$kota['nama'],'slug'=>Str::slug($kota['nama'], '-'));
            if(!empty($kota_id) && Str::slug($kota['nama'], '-') == $kota_id){
                $filter_kota = $kota['id'];
            }
        }
        if($request->has('filter_kota') && $request->filter_kota > 0){
            $filter_kota = $request->filter_kota;
        }
        if($request->has('filter_tanggal') && $request->filter_tanggal > 0){
            $filter_tanggal = Carbon::parse($request->filter_tanggal)->format('Y-m-d');
        }

        if(env('WIDGET_JADWAL_SHOLAT','off') == 'on'){
            $jadwal_sholat = JadwalSholatModel::where('tanggal',$filter_tanggal)->where('kota_external_id',$filter_kota)->first();
            if(!empty($jadwal_sholat)){
                $jadwal_sholat = $jadwal_sholat->toArray();
            }
            $jadwal_sholat_satu_bulan = JadwalSholatModel::whereRaw("date_format(tanggal,'%Y-%m') = '".$filter_tanggal_bulan."'")->where('kota_external_id',$filter_kota)->get();
            if(!empty($jadwal_sholat_satu_bulan)){
                $jadwal_sholat_satu_bulan = $jadwal_sholat_satu_bulan->toArray();
            }
        }
        $bulan = Carbon::parse($tanggal)->format('M');
        return view('front', compact('url','data_page','default_menus','jadwal_sholat','jadwal_sholat_satu_bulan','daftar_kota','filter_kota','bulan'));
    }

    public function pencarianJadwalSholat(Request $request){
        $url = $request->segment(1);
        if(empty($url)){
            $url = (env('DEFAULT_PAGE')!="")?'/'.env('DEFAULT_PAGE'):'/home';
        }else{
            $url = '/'.$url;
        }
        $data_page = PageModel::where('URL',$url)->first();
        $default_menus = PageMenuModel::where('is_default',1)->get();

        $jadwal_sholat = array();
        $jadwal_sholat_satu_bulan = array();
        $filter_tanggal = Carbon::now()->format('Y-m-d');
        $filter_tanggal_bulan = Carbon::now()->format('Y-m');

       
        $file_path = 'public/json/daftar_kota_mini.json';
        $daftar_kota_json = file_get_contents(base_path($file_path));
        $daftar_kota_json = json_decode($daftar_kota_json,true);
        if($request->has('filter_kota') && $request->filter_kota_bulan != null){
            $filter_kota = $request->filter_kota;
        }
        if($request->has('filter_kota_bulan') && $request->filter_kota_bulan != null){
            $filter_kota = $request->filter_kota_bulan;
           
        }
        if($request->has('filter_tanggal') && $request->filter_tanggal > 0 && $request->filter_tanggal !=null){
            $filter_tanggal = Carbon::parse($request->filter_tanggal)->format('Y-m-d');
        }
        if($request->has('filter_tanggal_bulan') && $request->filter_tanggal_bulan > 0 && $request->filter_tanggal_bulan !=null){
            $filter_tanggal = Carbon::parse($request->filter_tanggal_bulan)->format('Y-m-d');
        }
        foreach ($daftar_kota_json['kota'] as $kota) {
            $daftar_kota[] = array('id'=>$kota['id'],'nama'=>$kota['nama'],'slug'=>Str::slug($kota['nama'], '-'));
            if(!empty($filter_kota) && Str::slug($kota['nama'], '-') == $filter_kota){
                $filter_kota = $kota['id'];
            }
        }
        if(env('WIDGET_JADWAL_SHOLAT','off') == 'on'){

            $jadwal_sholat = JadwalSholatModel::where('tanggal',$filter_tanggal)->where('kota_external_id',$filter_kota)->first();
            if(!empty($jadwal_sholat)){
                $jadwal_sholat = $jadwal_sholat->toArray();
            }
            $jadwal_sholat_satu_bulan = JadwalSholatModel::where('kota_external_id',$filter_kota);
            if($filter_tanggal !="" && $filter_tanggal !=null){
                $tanggal_bulan = Carbon::parse($filter_tanggal)->format('Y-m');
                $jadwal_sholat_satu_bulan->whereRaw("date_format(tanggal,'%Y-%m') = '".$tanggal_bulan."'");
            }
            $jadwal_sholat_satu_bulan = $jadwal_sholat_satu_bulan->get();
            if(!empty($jadwal_sholat_satu_bulan)){
                $jadwal_sholat_satu_bulan = $jadwal_sholat_satu_bulan->toArray();
            }
        }
        $bulan = Carbon::parse($filter_tanggal)->format('M');
        return view('front', compact('url','data_page','default_menus','jadwal_sholat','jadwal_sholat_satu_bulan','daftar_kota','filter_kota','bulan'));
    }

    public function contentDetail($id){
        $content = ContentModel::where('ID',$id)->first();
        if(empty($content)){
            $content = ContentModel::where('slug',$id)->first();
        }
        $default_menus = PageMenuModel::where('is_default',1)->get();
        if(!empty($content)){
            return view('front_view.content', compact('content','default_menus'));
        }
    }

    public function getArtikel(Request $request){
        $response = $this->generateArtikel->getArtikelDownload($request);

        if($response['status']){
            return response()->json([
                'status' => true,
                'message' => 'Artikel berhasil di download'
            ]);
        }

        return response()->json([
            'status' => false,
            'message' => $response['message']
        ]);

    }
}
