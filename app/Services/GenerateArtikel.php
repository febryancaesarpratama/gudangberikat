<?php


namespace App\Services;

use App\Models\ContentMediaModel;
use App\Models\ContentModel;
use App\Models\PageLayoutContentModel;
use Carbon\Carbon;
use Illuminate\Support\Str;


class GenerateArtikel {
    public function getArtikelDownload($request){

        try{
            $url = 'https://ai.indonesiacore.com/api/export-artikel/2';
        $ch = curl_init($url);

        // curl_setopt($ch, CURLOPT_URL, 'https://chatgpt.febryancaesarpratama.com/api/text');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($prompt));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);

        if (curl_errno($ch)) {
            return [
                'status' => false,
                'message' => curl_error($ch)
            ];
        }
        
        curl_close($ch);
        // dd($response);
        // var_dump($response);

        $result = json_decode($response, true);

        // dd($result);

        if($result['result'] != null){

            foreach($result['result'] as $key){
                // dd($key['content']);
    
                $pregreplacetitle = str_replace('Title','', preg_replace("/[^a-zA-Z0-9_-]/", " ", $key['title']));
                $imagereplace = str_replace('public/images/', 'static/', $key['image']);
    
                $pregreplacecontent = str_replace('html','',str_replace('```', '', $key['content']));
    
                // dd($pregreplacecontent);
                $content = ContentModel::create([
                    'Title' => $pregreplacetitle,
                    'ShortContent' => $pregreplacecontent ?? 'Berikrat adalah sebuah platform yang menyediakan layanan pembuatan konten secara otomatis',
                    'Content' => $pregreplacecontent,
                    'slug' => Str::slug($pregreplacetitle, '-') ?? Str::slug('Berikrat adalah sebuah platform yang menyediakan layanan pembuatan konten secara otomatis', '-'),
                    'keyword' => "keyword",
                    'meta_description' => $key['title'],
                    'IsPublished' => 1,
                    'AddedTime' => Carbon::now()->timestamp ?? 0,
                    'IDPrevArticle' => 0,
                    'IDNextArticle' => 0,
    
                ]);
    
    
                ContentMediaModel::create([
                    'URL' => $imagereplace,
                    'SortNo' => 1,
                    'IDContent' => $content->ID,
                    'IsPrimary' => 1,
                    'AddedTime' => time(),
                    'AddedByIP' => $request->ip(),
                    'EditedTime' => time(),
                    'EditedByIP' => $request->ip(),
                    'Type' => 'Image'
                ]);
    
                PageLayoutContentModel::create([
                    'IDPage' => 2,
                    'TableNo' => 13,
                    'TableWidth' => NULL,
                    'TableHeight' => NULL,
                    'TableCSS' => 0,
                    'RowNo' => 0,
                    'RowWidth' => NULL,
                    'RowHeight' => NULL,
                    'RowCSS' => NULL,
                    'ColNo' => 0,
                    'ColWidth' => NULL,
                    'ColHeight' => NULL,
                    'ColCSS' => NULL,
                    'IDContent' => $content->ID,
                    'AddedTime' => Carbon::now()->timestamp,
                    'AddedByIP' => $request->ip(),
                    'EditedTime' => Carbon::now()->timestamp,
                    'EditedByIP' => $request->ip(),
                    'DivNo' => 13,
                    'DivWidth' => 100,
                    'DivHeight' => 40,
                    'DivCss' => '',
                    'DivClass' => 'col-md-12',
    
                ]);
    
                // dd($content);
    
                return [
                    'status' => true,
                    'message' => 'Artikel berhasil di download'
                ];
            }
        }else{
            return [
                'status' => false,
                'message' => 'Artikel tidak ditemukan'
            ];
        }


        }catch(\Exception $e){
            return [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        
    }
}