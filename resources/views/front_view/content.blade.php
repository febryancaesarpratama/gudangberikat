@extends('layout.front-master')
@section('meta')
@include('meta::manager', [
            'title'             => $content->Title,
            'description'       => $content->meta_description,
            'keywords'  => $content->keyword,
            'image'             => '',
        ])
@endsection
@section('content')
<div class="row" style="margin-top:5%;">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2>{{ $content->Title }}</h2>
            {{-- {{ dd($content) }} --}}
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">

                    @if(!empty($content->contentMedia->URL))
                                {{-- <img src="{{asset('storage/'.$content->contentMedia->URL)}}" alt="" width="300px" height="300px"> --}}
                                @if (str_contains($content->contentMedia->URL, 'static'))
                                      <img src="https://cronjob.archiloka.com/{{ $content->contentMedia->URL }}" class="img-rounded" width="300px" height="150px">
                                      @else
                                      {{-- "Gagal" --}}
                                      <img src="{{asset('storage/'.$content->contentMedia->URL)}}" class="img-rounded" width="300px" height="150px">
                                    @endif
                              @else
                                <img src="{{asset('images/no_image.jpg')}}" alt="" width="300px" height="300px">
                              @endif
                </div>
                <div class="col-md-8">
                    @php
                            $title = $content->Content;
                            $strrepl = str_replace('< lang="en">', '', $title);
                            $substr = str_replace('<h1>', '', $strrepl);
                            $substr2 = str_replace('</h1>', '', $substr);
                    @endphp
                    {!! $substr2 !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection