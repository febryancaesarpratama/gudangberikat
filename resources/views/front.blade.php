<!DOCTYPE html>
<html lang="en">
<head>
    @if(env('GOOGLE_ANALITYC') !="")
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer',"{{env('GOOGLE_ANALITYC')}}");</script>
    <!-- End Google Tag Manager -->
    @endif
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @if(env('HEAD_TITLE') !="")
      <title>{{env('HEAD_TITLE')}}</title>
    @elseif(env('HEAD_TITLE') =="")
      @if(!empty($content))
      <title>{{$content->Title}}</title>
      @else
      <title>{{env('APP_NAME')}}</title>
      @endif
    @endif
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    @include('meta::manager', [
        'title'         => (!empty($content->Title))?$content->Title:env('APP_NAME'),
        'description'   => (!empty($data_page->description))?$data_page->description:request()->segment(1),
        'keywords'      => (!empty($data_page->keyword))?$data_page->keyword:request()->segment(1)
    ])

    @if(env('META_DESCRIPTION') !="")
      <meta name="description" content="{{env('META_DESCRIPTION')}}">
    @endif

    @if(env('META_KEYWORDS') !="")
      <meta name="keywords" content="{{env('META_KEYWORDS')}}">
    @endif

    @if(env('META_geo.placename') !="")
      <meta name="geo.placename" content="{{env('META_geo.placename')}}">
    @endif
    @if(env('META_dcterms.Format') !="")
      <meta name="dcterms.Format" content="{{env('META_dcterms.Format')}}">
    @endif
    @if(env('META_dcterms.Language') !="")
      <meta name="dcterms.Language" content="{{env('META_dcterms.Language')}}">
    @endif
    @if(env('META_dcterms.Identifier') !="")
      <meta name="dcterms.Identifier" content="{{env('META_dcterms.Identifier')}}">
    @endif
    @if(env('META_dcterms.Relationr') !="")
      <meta name="dcterms.Relationr" content="{{env('META_dcterms.Relationr')}}">
    @endif
    @if(env('META_dcterms.Publisher') !="")
      <meta name="dcterms.Publisher" content="{{env('META_dcterms.Publisher')}}">
    @endif
    @if(env('META_dcterms.Type') !="")
      <meta name="dcterms.Type" content="{{env('META_dcterms.Type')}}">
    @endif
    @if(env('META_dcterms.Coverage') !="")
      <meta name="dcterms.Coverage" content="{{env('META_dcterms.Coverage')}}">
    @endif
    @if(env('META_dcterms.Title') !="")
      <meta name="dcterms.Title" content="{{env('META_dcterms.Title')}}">
    @endif
    @if(env('META_dcterms.Subject') !="")
      <meta name="dcterms.Subject" content="{{env('META_dcterms.Subject')}}">
    @endif
    @if(env('META_dcterms.Contributor') !="")
      <meta name="dcterms.Contributor" content="{{env('META_dcterms.Contributor')}}">
    @endif
    @if(env('META_dcterms.Description') !="")
      <meta name="dcterms.Description" content="{{env('META_dcterms.Description')}}">
    @endif

    <!-- Facebook OpenGraph -->
    @if(env('META_og_locale') !="")
      <meta property="og:locale" content="{{env('META_og_locale')}}">
    @endif
    @if(env('META_og_type') !="")
      <meta property="og:type" content="{{env('META_og_type')}}">
    @endif
    @if(env('META_og_url') !="")
      <meta property="og:url" content="{{env('META_og_url')}}">
    @endif
    @if(env('META_og_title') !="")
      <meta property="og:title" content="{{env('META_og_title')}}">
    @endif
    @if(env('META_og_description') !="")
      <meta property="og:description" content="{{env('META_og_description')}}">
    @endif
    @if(env('META_og_image') !="")
      <meta property="og:image" content="{{env('META_og_image')}}">
    @endif
    @if(env('META_og_site_name') !="")
      <meta property="og:site_name" content="{{env('META_og_site_name')}}">
    @endif
    

    <!-- Twitter Card -->
    @if(env('META_twitter_card') !="")
      <meta name="twitter:card" content="{{env('META_twitter_card')}}">
    @endif
    @if(env('META_twitter_title') !="")
      <meta name="twitter:title" content="{{env('META_twitter_title')}}">
    @endif
    @if(env('META_twitter_description') !="")
      <meta name="twitter:description" content="{{env('META_twitter_description')}}">
    @endif
    @if(env('META_twitter_image') !="")
      <meta name="twitter:image" content="{{env('META_twitter_image')}}">
    @endif

    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
</head>
<body>
    @if(env('GOOGLE_ANALITYC') !="")
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{env('GOOGLE_ANALITYC')}}"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @endif
    <div class="container">
      <div class="row">
        <div class="col-md-12">
        </div>
        <nav class="navbar navbar-default">
          <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <h1><b class="text-primary">{{env('APP_NAME')}}</b></h1>
              </div>
          </div>
          <div class="container-fluid">
            <!-- Collect the nav links, forms, and other content for toggling -->
            @if(empty($data_page->menus) || count($data_page->menus) == 0)
              @if($default_menus->isEmpty())
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li {{ (request()->segment(1) == 'home' || request()->segment(1) == '') ? 'class=active' : '' }} ><a href="{{url('/home')}}">Home</a></li>
                  <li {{ (request()->segment(1) == 'about_us') ? 'class=active' : '' }} ><a href="{{url('/about_us')}}">About Us</a></li>
                  <li {{ (request()->segment(1) == 'services') ? 'class=active' : '' }} ><a href="{{url('/services')}}">Services</a></li>
                  <li {{ (request()->segment(1) == 'products') ? 'class=active' : '' }} ><a href="{{url('/products')}}">Products</a></li>
                  <li {{ (request()->segment(1) == 'clients') ? 'class=active' : '' }} ><a href="{{url('/clients')}}">Clients</a></li>
                  <li {{ (request()->segment(1) == 'supports') ? 'class=active' : '' }} ><a href="{{url('/supports')}}">Supports</a></li>
                </ul>
                
              </div><!-- /.navbar-collapse -->
              @elseif(!$default_menus->isEmpty())
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                    @foreach($default_menus as $menu)
                      <li {{ (request()->segment(1) == $menu->menu_name ) ? 'class=active' : '' }} ><a href="{{url($menu->menu_url)}}">{{$menu->menu_name}}</a></li>
                    @endforeach
                  </ul>
                  
                </div><!-- /.navbar-collapse -->
              @endif
            @elseif(!empty($data_page->menus) && count($data_page->menus) > 0)
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  @foreach($data_page->menus as $menu)
                    <li {{ (request()->segment(1) == $menu->menu_name ) ? 'class=active' : '' }} ><a href="{{url($menu->menu_url)}}">{{$menu->menu_name}}</a></li>
                  @endforeach
                </ul>
                
              </div><!-- /.navbar-collapse -->
            @endif
          </div><!-- /.container-fluid -->
        </nav>
      <!-- Example row of columns -->
      @if(env('WIDGET_JADWAL_SHOLAT','off') == 'on')
        <div class="row">
          <div class="col-md-12">
                  {!! Form::open(['url' => '/jadwalsholat','id'=>"form-widget-jadwal-sholat-bulan", 'method' => 'get']) !!}
                  <div class="visible-md visible-lg">
                    <table class="table">
                        <tr>
                            <td colspan="4">
                              <span class="pull-right">Filter</span>
                            </td>
                            <td colspan="2">
                              <input type="text" name="filter_tanggal_bulan" id="filter_tanggal_bulan" value="{{Request::get('filter_tanggal_bulan')}}" class="form-control">
                            </td>
                            <td colspan="2">
                              <select name="filter_kota_bulan" id="filter_kota_bulan" class="form-control select2 filter_kota_bulan">
                                @foreach($daftar_kota as $v)
                                  @if(Request::get('filter_kota_bulan') == "")
                                    <option value="{{$v['slug']}}" @if($v['id'] == 667) selected @endif>{{$v['nama']}}</option>
                                  @elseif(Request::get('filter_kota_bulan') !="")
                                    <option value="{{$v['slug']}}" @if($v['slug'] == Request::get('filter_kota_bulan')) selected @endif>{{$v['nama']}}</option>
                                  @endif
                                @endforeach
                              </select>
                            </td>
                            <td>
                              <a href="#" id="btn_cari" class="btn btn-sm btn-primary">Cari</a>
                            </td>
                        </tr>
                    </table>
                  </div>
                  {!! Form::close() !!}
          </div>
        </div>
      @endif
      <div class="row">
        @if(!empty($data_page->contentLayouts))
          @foreach($data_page->contentLayouts as $v)
              @if($v->DivClass !="")
                  <div class="{{$v->DivClass}}">
              @else
                  <div class="col-md-10">
              @endif
                   @if(!empty($v->content))
                      
                      @if(!empty($v->DivClass) && ( $v->DivClass == 'col-md-7' || $v->DivClass == 'col-md-8' || $v->DivClass == 'col-md-9' || $v->DivClass == 'col-md-9' || $v->DivClass == 'col-md-10' || $v->DivClass == 'col-md-11' || $v->DivClass == 'col-md-12'))
                        <h2>
                          @if($v->content->slug !="")
                            <a href="{{url(route('front.content.detail',$v->content->slug))}}">
                          @else
                            <a href="{{url(route('front.content.detail',$v->content->ID))}}">
                          @endif
                            {{ $v->content->Title }}
                          </a>
                        </h2>
                        <div class="col-md-4">
                          @if(!empty($v->content->contentMedia->URL))
                            @if (str_contains($v->content->contentMedia->URL, 'static'))
                              <img src="https://cronjob.archiloka.com/{{ $v->content->contentMedia->URL }}" class="img-rounded" width="300px" height="150px">
                              @else
                              {{-- "Gagal" --}}
                              <img src="{{asset('storage/'.$v->content->contentMedia->URL)}}" class="img-rounded" width="300px" height="150px">
                            @endif
                            {{-- <img src="{{asset('storage/'.$v->content->contentMedia->URL)}}" class="img-rounded" width="300px" height="150px"> --}}
                          @else
                            <img src="{{asset('images/no_image.jpg')}}" alt="" width="300px" height="150px">
                          @endif
                        </div>
                        <div class="col-md-8">
                          <span>
                            {{-- @php

                              // if (condition) {
                              //   # code...
                              // }
                                $title = $v->content->Content;
                                $strrepl = str_replace('< lang="en">', '', $title);
                                $substr = substr($strrepl, 0, 700);
                                $substr1 = str_replace('<h1>', '', $substr);
                                $substr2 = str_replace('</h1>', '', $substr1);
                            @endphp --}}

                            {{-- {!! $substr2 !!} --}}
                            {{-- {!! $v->content->ShortContent !!} --}}
                            @php
                            $shortContent = $v->content->Content;
                            $strstriptags = strip_tags($shortContent);
                            $strrepl = str_replace('< lang="en">', '', $strstriptags);
                            $substr = substr($strrepl, 0, 350);
                            
                          @endphp
                          {{ $substr }}
                          </span>
                        </div>
                      @elseif(!empty($v->DivClass) && ($v->DivClass == 'col-md-1' || $v->DivClass == 'col-md-2' || $v->DivClass == 'col-md-3' || $v->DivClass == 'col-md-4' || $v->DivClass == 'col-md-5' || $v->DivClass == 'col-md-6'))
                        <div class="col-sm-12">
                          @if(!empty($v->content->contentMedia->URL))
                            <img src="{{asset('storage/'.$v->content->contentMedia->URL)}}" class="img-rounded" width="300px" height="150px">
                          @else
                            <img src="{{asset('images/no_image.jpg')}}" alt="" width="300px" height="150px">
                          @endif
                        </div>
                        <h2>
                          @if($v->content->slug !="")
                            <a href="{{url(route('front.content.detail',$v->content->slug))}}">
                          @else
                            <a href="{{url(route('front.content.detail',$v->content->ID))}}">
                          @endif
                            {{ $v->content->Title }}
                          </a>
                        </h2>
                        <div class="col-sm-12">
                          {{ $v->content->ShortContent }}
                        </div>
                      @else
                        <div class="col-sm-12">
                          @if(!empty($v->content->contentMedia->URL))
                            <img src="{{asset('storage/'.$v->content->contentMedia->URL)}}" class="img-rounded" width="300px">
                          @else
                            <img src="{{asset('images/no_image.jpg')}}" alt="" width="300px" height="300px">
                          @endif
                        </div>
                        <div class="col-sm-12">
                          {{ $v->content->ShortContent }}
                        </div>
                      @endif
                   @endif
                  </div>
          @endforeach
        @endif
      </div>

      <div class="row">
        @if(!empty($ads))
          @foreach($ads as $ad)
              @if($ad->boostrap_class !="")
                  <div class="{{$ad->boostrap_class}}">
              @else
                  <div class="col-md-6">
              @endif
                  <div class="row">
                  <h4>{{$ad->Title}}</h4><br>
                    @if($ad->DestinationURL != "")
                      <a href="{{$ad->DestinationURL}}" target="_blank">
                    @elseif($ad->DestinationURL == "")
                      <a href="#" target="_blank">
                    @endif
                      <img src="{{asset('storage/'.$ad->media->Location)}}" class="img-rounded img-fluid" width="100%" style="padding-left:20px; padding-right:20px;">
                    </a>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      {!! $ad->ShortContent !!}
                    </div>
                  </div>
            </div>
          @endforeach
        @endif
      </div>
      <div class="row" style="margin-top:20px;">
        <div class="col-md-6">
            <div class="row">
              <div class="img"><img src="{{asset('images/ads_space.png')}}" alt="space iklan" class="img-thumbnail"></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
              <div class="img"><img src="{{asset('images/ads_space.png')}}" alt="space iklan" class="img-thumbnail"></div>
            </div>
        </div>
      </div>

      <hr>

    </div> <!-- /container -->

    
     <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
     <!-- Latest compiled and minified JavaScript -->
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    
     <script
      src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
      integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
      crossorigin="anonymous"></script>

      <script>
        $( function() {
          $( "#filter_tanggal" ).datepicker({
            dateFormat:'dd-mm-yy'
          });
          $( "#filter_tanggal_bulan" ).datepicker({
            dateFormat:'dd-mm-yy'
          });
        } );
        $("#filter_kota").change(function(){
          $('#form-widget-jadwal-sholat').submit();
        });
        $('#btn_cari').on('click', function(){
          $('#form-widget-jadwal-sholat-bulan').submit();
        });
        // $('.select2').select2();
        $('.select23').select2();

        $(".filter_kota_bulan").select2({
            tags: true,
            placeholder: "Pilih Kota",
        });
        $(".filter_kota_bulan_xs").select2({
            tags: true,
            placeholder: "Pilih Kota",
        });
      </script>
  
</body>
</html>