@if ($message = Session::get('success'))
<div class="alert alert-success">
	<strong>{{ trans('message.success') }}!</strong> {{ $message }}
</div>
@endif

@if ($message = Session::get('error'))
<div class="alert alert-danger">
	<strong>{{ trans('message.error') }}!</strong>{{ $message }}
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning">
	<strong>{{ trans('message.error') }}!</strong>{{ $message }}
</div>
@endif

@if ($message = Session::get('info'))
<div class="alert alert-info">
	<strong>{{ trans('message.error') }}!</strong>{{ $message }}
</div>
@endif

@if ($errors->any())
<div class="alert alert-danger">
	<strong>{{ trans('message.error') }}!</strong>{{ $message }}
</div>
@endif	