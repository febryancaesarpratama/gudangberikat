<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @yield('meta')
    @if(!empty($content))
    <title>{{$content->Title}}</title>
    @else
    <title>{{env('APP_NAME')}}</title>
    @endif
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    @yield('css')
    
    @include('meta::manager', [
        'title'         => (!empty($content->Title))?$content->Title:env('APP_NAME'),
        'description'   => (!empty($content->ShortContent))?$content->ShortContent:''
    ])
</head>
<body>
    

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="container">
      <div class="row">
          <div class="col-md-12">
          </div>
          <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <h1><b class="text-primary">{{env('APP_NAME')}}</b></h1>
                </div>
            </div>
            <div class="container-fluid">
              <!-- Collect the nav links, forms, and other content for toggling -->
                @if(empty($default_menus))
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                    <li {{ (request()->segment(1) == 'home' || request()->segment(1) == '') ? 'class=active' : '' }} ><a href="{{url('/home')}}">Home</a></li>
                    <li {{ (request()->segment(1) == 'about_us') ? 'class=active' : '' }} ><a href="{{url('/about_us')}}">About Us</a></li>
                    <li {{ (request()->segment(1) == 'services') ? 'class=active' : '' }} ><a href="{{url('/services')}}">Services</a></li>
                    <li {{ (request()->segment(1) == 'products') ? 'class=active' : '' }} ><a href="{{url('/products')}}">Products</a></li>
                    <li {{ (request()->segment(1) == 'clients') ? 'class=active' : '' }} ><a href="{{url('/clients')}}">Clients</a></li>
                    <li {{ (request()->segment(1) == 'supports') ? 'class=active' : '' }} ><a href="{{url('/supports')}}">Supports</a></li>
                  </ul>
                </div><!-- /.navbar-collapse -->
                @elseif(!empty($default_menus))
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                      @foreach($default_menus as $menu)
                        <li {{ (request()->segment(1) == $menu->menu_name ) ? 'class=active' : '' }} ><a href="{{url($menu->menu_url)}}">{{$menu->menu_name}}</a></li>
                      @endforeach
                    </ul>
                    
                  </div><!-- /.navbar-collapse -->
                @endif
            </div><!-- /.container-fluid -->
          </nav>
        <!-- Example row of columns -->
      <div class="row">
        @yield('content')
      </div>
    </div> <!-- /container -->

    
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
     <!-- Latest compiled and minified JavaScript -->
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    @yield('js')
</body>
</html>