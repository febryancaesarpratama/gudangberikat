<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <meta name="csrf_token" content="{{csrf_token()}}">

        <link rel="shortcut icon" href="{{asset('ubold/assets/images/favicon.ico')}}">

        <title>Ubold - Responsive Admin Dashboard Template</title>

        <link href="{{asset('ubold/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('ubold/assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('ubold/assets/css/style.css')}}" rel="stylesheet" type="text/css" />

        <script src="{{asset('ubold/assets/js/modernizr.min.js')}}"></script>
        
    </head>
    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="card-box">
                <div class="panel-heading">
                    <h4 class="text-center"> {{ __('login.title_sign') }} <strong class="text-custom"></strong></h4>
                </div>


                <div class="p-20">
                    <form class="form-horizontal m-t-20" action="{{ route('admin.login') }}" method="post">
                        @csrf
                        <div class="form-group ">
                            <div class="col-12">
                                
                                <input class="form-control" name="email" type="text" required="" placeholder="{{ __('login.input_username') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-12">
                                <input class="form-control" name="password" type="password" required="" placeholder="{{ __('login.input_password') }}">
                            </div>
                        </div>

                        <div class="form-group ">
                            <div class="col-12">
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox-signup" type="checkbox">
                                    <label for="checkbox-signup">
                                        {{ __('login.remember_me') }}
                                    </label>
                                </div>

                            </div>
                        </div>

                        <div class="form-group text-center m-t-40">
                            <div class="col-12">
                                <button class="btn btn-pink btn-block text-uppercase waves-effect waves-light"
                                        type="submit">{{ __('login.btn_login') }}
                                </button>
                            </div>
                        </div>

                        <div class="form-group m-t-30 m-b-0">
                            <div class="col-12">
                                <a href="page-recoverpw.html" class="text-dark"><i class="fa fa-lock m-r-5"></i> {{ __('login.text_forget_password') }}</a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <p>{{ __('login.text_for_register') }} <a href="page-register.html" class="text-primary m-l-5"><b>{{ __('login.btn_signup') }}</b></a>
                    </p>

                </div>
            </div>
            
        </div>
        
        

        
    	<script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="{{asset('ubold/assets/js/jquery.min.js')}}"></script>
        <script src="{{asset('ubold/assets/js/popper.min.js')}}"></script><!-- Popper for Bootstrap -->
        <script src="{{asset('ubold/assets/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('ubold/assets/js/detect.js')}}"></script>
        <script src="{{asset('ubold/assets/js/fastclick.js')}}"></script>
        <script src="{{asset('ubold/assets/js/jquery.slimscroll.js')}}"></script>
        <script src="{{asset('ubold/assets/js/jquery.blockUI.js')}}"></script>
        <script src="{{asset('ubold/assets/js/waves.js')}}"></script>
        <script src="{{asset('ubold/assets/js/wow.min.js')}}"></script>
        <script src="{{asset('ubold/assets/js/jquery.nicescroll.js')}}"></script>
        <script src="{{asset('ubold/assets/js/jquery.scrollTo.min.js')}}"></script>

        <script src="{{asset('ubold/assets/js/jquery.core.js')}}"></script>
        <script src="{{asset('ubold/assets/js/jquery.app.js')}}"></script>
	
	</body>
</html>