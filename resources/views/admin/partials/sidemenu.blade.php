<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
                <li class="text-muted menu-title">Navigation</li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="#">Dashboard 1</a></li>
                        <li><a href="#">Dashboard 2</a></li>
                        <li><a href="dashboard_3.html">Dashboard 3</a></li>
                        <li><a href="dashboard_4.html">Dashboard 4</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-users"></i> <span> Users </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        
                            <li><a href="{{ url(route('admin.user.index')) }}" class="{{ (request()->segment(2) == 'user') ? 'active' : '' }}">User Management</a></li>
  
                  
                            <li><a href="{{ url(route('admin.permission.index')) }}" class="{{ (request()->segment(2) == 'permission') ? 'active' : '' }}">Permission</a></li>
  
          
                            <li><a href="{{ url(route('admin.role.index')) }}" class="{{ (request()->segment(2) == 'role') ? 'active' : '' }}">Role</a></li>
  
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-newspaper-o"></i> <span> Content </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{url(route('admin.content.index'))}}">Content Management</a></li>
                        <li><a href="{{url(route('admin.page.index'))}}">Page Management</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-newspaper-o"></i> <span> Ads </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{url(route('admin.adsmanager.index'))}}">Ads Management</a></li>
                    </ul>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>