@extends('layout.master')
@section('content')

<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Datatable</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Ubold</a></li>
                    <li class="breadcrumb-item"><a href="#">Page</a></li>
                    <li class="breadcrumb-item active">Edit</li>
                </ol>

            </div>
        </div>
        {!! Form::model($listdata, ['method' => 'PATCH','route' => ['admin.page.update', $listdata->ID]]) !!}
        <div class="row">
            <div class="col-12">
                <div class="card-box table-responsive">
                    <h4 class="m-t-0 header-title">Edit Data Page</h4>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Name</label>
                        <div class="col-10">
                            {!! Form::text('Name', null, array('placeholder' => 'input Name','class' => 'form-control')) !!}
                            @if ($errors->has('Name'))
                                <p class="text-danger">{{ $errors->first('Name') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">URL</label>
                        <div class="col-10">
                            {!! Form::text('URL', null, array('placeholder' => 'input URL','class' => 'form-control','id'=>'URL')) !!}
                            @if ($errors->has('URL'))
                                <p class="text-danger">{{ $errors->first('URL') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Description</label>
                        <div class="col-10">
                            {!! Form::textarea('description', null, array('placeholder' => 'input description','class' => 'form-control','id'=>'description')) !!}
                            @if ($errors->has('description'))
                                <p class="text-danger">{{ $errors->first('description') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Keyword</label>
                        <div class="col-10">
                            {!! Form::textarea('keyword', null, array('placeholder' => 'input keyword','class' => 'form-control','id'=>'keyword')) !!}
                            @if ($errors->has('keyword'))
                                <p class="text-danger">{{ $errors->first('keyword') }}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="m-t-0 header-title">Menus</h4>
                            <div class="form-group">
                                <table class="">
                                    <tr>
                                        <td>
                                            <input type="text" name="menu_name" id="menu_name" class="form-control" placeholder="menu name">
                                        </td>
                                        <td>
                                            <input type="text" name="menu_url" id="menu_url" class="form-control" placeholder="url">
                                        </td>
                                        <td>
                                            Is Default <input type="checkbox" name="menu_default" id="menu_default" @if($is_default == 1)checked @endif>
                                        </td>
                                        <td>
                                            @if(!empty($menu_items))
                                                <a href="#tabel-body-menu" class="btn btn-primary btn-sm" id="btn-add-menu" data-no="{{$menu_items->count()+1}}"><i class="fa fa-plus"></i></a>
                                            @else
                                                <a href="#tabel-body-menu" class="btn btn-primary btn-sm" id="btn-add-menu" data-no="1"><i class="fa fa-plus"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="form-group">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Menu name</th>
                                            <th>Url Address</th>
                                            <th>Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tabel-body-menu">
                                        @if(!empty($menu_items))
                                            @foreach($menu_items as $item)
                                                <tr id="menu-{{$loop->iteration}}">
                                                    <td>
                                                        {{$item->menu_name}}
                                                        <input type="hidden" name="menu_items[{{$loop->iteration}}]" value="{{$item->ID}}" id="menu-id-{{$loop->iteration}}">
                                                        <input type="hidden" name="menu_items[{{$loop->iteration}}][menu_name]" value="{{$item->menu_name}}">
                                                    </td>
                                                    <td>
                                                        {{$item->menu_url}}
                                                        <input type="hidden" name="menu_items[{{$loop->iteration}}][menu_url]" value="{{$item->menu_url}}">
                                                    </td>
                                                    <td>
                                                        <a href="#tabel-body-menu" class="btn btn-danger btn-sm" onclick="removeMenu({{$loop->iteration}})"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                            <hr>
                            <!-- input menu dinamis -->
                            <div class="form-group">
                                <!-- <label class="col-2 col-form-label" >Last Name</label> -->
                                <div class="col-10">
                                    <button class="btn btn-danger waves-effect waves-light"> <i class="fa fa-close m-r-5"></i> <span>Cancel</span> </button>
                                    <button class="btn btn-default waves-effect waves-light"> <i class="fa fa-heart m-r-5"></i> <span>Submit</span> </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div> <!-- end card-box -->
            </div> <!-- end col -->
        </div>
        {!! Form::close() !!}
    </div> <!-- container -->

</div> <!-- content -->

@endsection
@section('js')
    <script>
        var removeMenu = function(id){
            var menuId = $('#menu-id-'+id).val();
            var token = $("meta[name='csrf-token']").attr("content");
            $('#menu-'+id).remove();
            $.ajax({
                url:"{{url('admin/page/delete_menu')}}/"+menuId,
                type: 'DELETE',
                data: {
                    "id": menuId,
                    "_token": token,
                },
                success:function(data)
                {

                }
            })
        }
        $('#btn-add-menu').click(function(){
            var menu_name = $('#menu_name').val();
            var menu_url = $('#menu_url').val();
            var kunci =  $('#btn-add-menu').attr('data-no');
            var html = '';
            html +='<tr id="menu-'+kunci+'">';
                html +='<td>'
                    html += menu_name;
                    html +='<input type="hidden" name="menu_items['+kunci+'][menu_name]" value="'+menu_name+'">';
                    html +='</td>';
                html +='<td>';
                    html += menu_url;
                    html +='<input type="hidden" name="menu_items['+kunci+'][menu_url]" value="'+menu_url+'">';
                html +='</td>';
                html +='<td>';
                    html +='<a href="#tabel-body-menu" class="btn btn-danger btn-sm" onclick="removeMenu('+kunci+')"><i class="fa fa-trash"></i></a>';
                html +='</td>';
            html +='</tr>';
            $('#menu_name').val('');
            $('#menu_url').val('');
            kunci = parseInt(kunci)+1;
            $('#btn-add-menu').attr('data-no',kunci);
            $('#tabel-body-menu').append(html);
        });
    </script>
@endsection
