@extends('layout.master')
@section('content')

<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Datatable</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Ubold</a></li>
                    <li class="breadcrumb-item"><a href="#">Page</a></li>
                    <li class="breadcrumb-item"><a href="#">Pagelayoutcontent</a></li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>

            </div>
        </div>

        {!! Form::open(array('route' => 'admin.pagelayoutcontent.store','method'=>'POST')) !!}
        <div class="row">
            <div class="col-12">
                <div class="card-box table-responsive">
                    <h4 class="m-t-0 header-title">Add Data Page</h4>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Table No</label>
                        <div class="col-10">
                            {!! Form::text('TableNo', null, array('placeholder' => 'input TableNo','class' => 'form-control')) !!}
                            @if ($errors->has('TableNo'))
                                <p class="text-danger">{{ $errors->first('TableNo') }}</p>
                            @endif
                            <input type="hidden" name="IDPage" value="{{$id}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Content</label>
                        <div class="col-10">
                                {!! Form::select('IDContent', $contents,[], array('class' => 'select2 form-control')) !!}
                                @if ($errors->has('IDContent'))
                                    <p class="text-danger">{{ $errors->first('IDContent') }}</p>
                                @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Div No</label>
                        <div class="col-10">
                            {!! Form::text('DivNo', null, array('placeholder' => 'input DivNo','class' => 'form-control')) !!}
                            @if ($errors->has('DivNo'))
                                <p class="text-danger">{{ $errors->first('DivNo') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Div Width</label>
                        <div class="col-10">
                            {!! Form::text('DivWidth', null, array('placeholder' => 'input Div Width','class' => 'form-control')) !!}
                            @if ($errors->has('DivWidth'))
                                <p class="text-danger">{{ $errors->first('DivWidth') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Div Height</label>
                        <div class="col-10">
                            {!! Form::text('DivHeight', null, array('placeholder' => 'input Div Height','class' => 'form-control')) !!}
                            @if ($errors->has('DivHeight'))
                                <p class="text-danger">{{ $errors->first('DivHeight') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Div CSS</label>
                        <div class="col-10">
                            {!! Form::textarea('DivCss', null, array('placeholder' => 'input Div CSS','class' => 'form-control','id'=>'DivCss')) !!}
                            @if ($errors->has('DivCss'))
                                <p class="text-danger">{{ $errors->first('DivCss') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Div Class</label>
                        <div class="col-10">
                            {!! Form::textarea('DivClass', null, array('placeholder' => 'input Div Class','class' => 'form-control','id'=>'DivClass')) !!}
                            @if ($errors->has('DivClass'))
                                <p class="text-danger">{{ $errors->first('DivClass') }}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end row -->

        <!-- menu dinamis -->
        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="m-t-0 header-title">Menus</h4>
                            <div class="form-group">
                                <table class="">
                                    <tr>
                                        <td>
                                            <input type="text" name="menu_name" id="menu_name" class="form-control" placeholder="menu name">
                                        </td>
                                        <td>
                                            <input type="text" name="menu_url" id="menu_url" class="form-control" placeholder="url">
                                        </td>
                                        <td>
                                            <a href="#tabel-body-menu" class="btn btn-primary btn-sm" id="btn-add-menu" data-no="1"><i class="fa fa-plus"></i></a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="form-group">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Menu name</th>
                                            <th>Url Address</th>
                                            <th>Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tabel-body-menu">
                                        <!-- <tr id="">
                                            <td>
                                                <input type="hidden" name="menu_item[][menu_name]">
                                            </td>
                                            <td>
                                                <input type="hidden" name="menu_item[][menu_url]">
                                            </td>
                                            <td>
                                                <button class="btn btn-danger btn-sm" data-no="1"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr> -->
                                    </tbody>
                                </table>
                            </div>

                            <hr>
                            <!-- input menu dinamis -->
                            <div class="form-group">
                                <!-- <label class="col-2 col-form-label" >Last Name</label> -->
                                <div class="col-10">
                                    <button class="btn btn-danger waves-effect waves-light"> <i class="fa fa-close m-r-5"></i> <span>Cancel</span> </button>
                                    <button class="btn btn-default waves-effect waves-light"> <i class="fa fa-heart m-r-5"></i> <span>Submit</span> </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div> <!-- end card-box -->
            </div> <!-- end col -->
        </div>

        {!! Form::close() !!}

    </div> <!-- container -->

</div> <!-- content -->

@endsection
@section('js')
    <script src="{{asset('plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('plugins/edit_area/edit_area_full.js')}}"></script>

    <script>
        editAreaLoader.init({
            id : "DivCss"		// textarea id
            ,syntax: "css"			// syntax to be uses for highgliting
            ,start_highlight: true		// to display with highlight mode on start-up
        });
        editAreaLoader.init({
            id : "DivClass"		// textarea id
            ,syntax: "css"			// syntax to be uses for highgliting
            ,start_highlight: true		// to display with highlight mode on start-up
        });
        var removeMenu = function(id){
            $('#menu-'+id).remove();
        }
        $('#btn-add-menu').click(function(){
            var menu_name = $('#menu_name').val();
            var menu_url = $('#menu_url').val();
            var kunci =  $('#btn-add-menu').attr('data-no');
            var html = '';
            html +='<tr id="menu-'+kunci+'">';
                html +='<td>'
                    html += menu_name;
                    html +='<input type="hidden" name="menu_items['+kunci+'][menu_name]" value="'+menu_name+'">';
                    html +='</td>';
                html +='<td>';
                    html += menu_url;
                    html +='<input type="hidden" name="menu_items['+kunci+'][menu_url]" value="'+menu_url+'">';
                html +='</td>';
                html +='<td>';
                    html +='<a href="#tabel-body-menu" class="btn btn-danger btn-sm" onclick="removeMenu('+kunci+')"><i class="fa fa-trash"></i></a>';
                html +='</td>';
            html +='</tr>';
            $('#menu_name').val('');
            $('#menu_url').val('');
            kunci = parseInt(kunci)+1;
            $('#btn-add-menu').attr('data-no',kunci);
            $('#tabel-body-menu').append(html);
        });
    </script>
@endsection
