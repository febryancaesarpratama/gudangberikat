@extends('layout.master')
@section('css')
<link href="{{ asset('plugins/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        @include('flash-message')
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">
                    <a href="{{url(route('admin.role.create'))}}" class="btn btn-success waves-effect waves-light">
                        <i class="fa fa-plus"></i> Role
                    </a>
                </div>

                <h4 class="page-title">Datatable</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Ubold</a></li>
                    <li class="breadcrumb-item"><a href="#">Tables</a></li>
                    <li class="breadcrumb-item active">Datatable</li>
                </ol>

            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card-box table-responsive">
                    <h4 class="m-t-0 header-title">Data Role</h4>
                    <table id="datatableRole" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                          <tr>
                              <td colspan="2"></td>
                          </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end row -->

    </div> <!-- container -->

</div> <!-- content -->

    <!--  Modal content for the above example -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="confirmModal" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmation Delete Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h6>Are you sure want to delete this data ?</h6>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@section('js')
    <!-- Required datatable js -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>

    <!-- Buttons examples -->
    <script src="{{ asset('plugins/datatables/dataTables.buttons.min.js')}}"></script>

    <script>
    $(document).ready(function(){
        var dataTable = $('#datatableRole').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            language: {
                searchPlaceholder: '{{ trans('form.text_search') }}',
                sSearch: '',
                lengthMenu: '_MENU_ items/{{ trans('form.text_page') }}',
            },
            ajax: {
                url: "{{ route('admin.role.index') }}",
                data: function(data){
                    // var role = $('#role').val();
                    // var status = $('#status').val();
                    // var date_start = $('#date_start').val();
                    // var date_end = $('#date_end').val();

                    // data.role = role;
                    // data.status = status;
                    // if(date_start !="" && date_end !=""){
                    //     data.searchByDateStart = date_start;
                    //     data.searchByDateEnd = date_end;
                    // }
                    
                }
            },
            columns: [
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                }
            ],
            columnDefs: [
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap width-150'>" + data + "</div>";
                    },
                    targets: 0
                }
             ]
        });
        // $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        var listid;
        
        $(document).on('click', '.delete', function(){
            listid = $(this).attr('id');
            $('#confirmModal').modal('show');
        });
        
        $('#ok_button').click(function(){
            var token = $("meta[name='csrf-token']").attr("content");
            $.ajax({
                url:"role/"+listid,
                type: 'DELETE',
                data: {
                    "id": listid,
                    "_token": token,
                },
                beforeSend:function(){
                    $('#ok_button').text('{{ trans('form.text_deleting') }}');
                },
                success:function(data)
                {
                    setTimeout(function(){
                        $('#confirmModal').modal('hide');
                        $('#datatableRole').DataTable().ajax.reload();
                        $('#ok_button').text('OK');
                    }, 500);
                }
            })
        });

    });
    </script>
@endsection