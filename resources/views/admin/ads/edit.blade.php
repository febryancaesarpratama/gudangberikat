@extends('layout.master')
@section('content')

<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Datatable</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Ubold</a></li>
                    <li class="breadcrumb-item"><a href="#">Content</a></li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>

            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card-box table-responsive">
                    <h4 class="m-t-0 header-title">Add Data Ads</h4>
                    {!! Form::model($listdata, ['method' => 'PATCH','route' => ['admin.adsmanager.update', $listdata->id],'enctype'=>'multipart/form-data']) !!}
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Title</label>
                        <div class="col-10">
                            {!! Form::text('Title', null, array('placeholder' => 'input Title','class' => 'form-control')) !!}
                            @if ($errors->has('Title'))
                                <p class="text-danger">{{ $errors->first('Title') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Upload File</label>
                        <div class="col-6">
                            <input type="file" name="upload_file" id="upload_file" class="form-control">
                        </div>
                        <div class="col-6">
                            <img class="" id="img-preview" src="{{asset('design/img/picture/default.jpg')}}" alt="preview" height="210"> 
                            <p class="mg-t-10 tx-cente tx-13">Image Preview</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Div Class (untuk menentukan ukuran iklan)</label>
                        <div class="col-10">
                            {!! Form::select('boostrap_class', $list_ukuran, $listdata->boostrap_class,['class' => 'form-control']) !!}
                            @if ($errors->has('boostrap_class'))
                                <p class="text-danger">{{ $errors->first('boostrap_class') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">ShortContent</label>
                        <div class="col-10">
                            {!! Form::text('ShortContent', null, array('placeholder' => 'input ShortContent','class' => 'form-control')) !!}
                            @if ($errors->has('ShortContent'))
                                <p class="text-danger">{{ $errors->first('ShortContent') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Content</label>
                        <div class="col-10">
                            {!! Form::textarea('Content', null, array('placeholder' => 'input Content','class' => 'form-control','id'=>'Content')) !!}
                            @if ($errors->has('Content'))
                                <p class="text-danger">{{ $errors->first('Content') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Location</label>
                        <div class="col-10">
                            {!! Form::textarea('Location', null, array('placeholder' => 'input Location','class' => 'form-control','id'=>'Location')) !!}
                            @if ($errors->has('Location'))
                                <p class="text-danger">{{ $errors->first('Location') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">IsPrimary</label>
                        <div class="col-10">
                            {!! Form::select('IsPrimary', ['n'=>'Not Primary','y'=>'Primary'], $listdata->IsPrimary,['class' => 'form-control']) !!}
                            @if ($errors->has('IsPrimary'))
                                <p class="text-danger">{{ $errors->first('IsPrimary') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">DestinationURL</label>
                        <div class="col-10">
                            {!! Form::text('DestinationURL', null, array('placeholder' => 'input DestinationURL','class' => 'form-control')) !!}
                            @if ($errors->has('DestinationURL'))
                                <p class="text-danger">{{ $errors->first('DestinationURL') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">IDContent</label>
                        <div class="col-10">
                            {!! Form::text('IDContent', null, array('placeholder' => 'input IDContent','class' => 'form-control')) !!}
                            @if ($errors->has('IDContent'))
                                <p class="text-danger">{{ $errors->first('IDContent') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">StartPeriod</label>
                        <div class="col-10">
                            {!! Form::date('StartPeriod', $startPeriod, array('placeholder' => 'input StartPeriod','class' => 'form-control', 'id'=>'StartPeriod')) !!}
                            @if ($errors->has('StartPeriod'))
                                <p class="text-danger">{{ $errors->first('StartPeriod') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">EndPeriod</label>
                        <div class="col-10">
                            {!! Form::date('EndPeriod', $endPeriod, array('placeholder' => 'input EndPeriod','class' => 'form-control','id'=>'EndPeriod')) !!}
                            @if ($errors->has('EndPeriod'))
                                <p class="text-danger">{{ $errors->first('EndPeriod') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Client Name</label>
                        <div class="col-10">
                            {!! Form::text('Name', null, array('placeholder' => 'input Name','class' => 'form-control')) !!}
                            @if ($errors->has('Name'))
                                <p class="text-danger">{{ $errors->first('Name') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Client Email</label>
                        <div class="col-10">
                            {!! Form::text('Email', null, array('placeholder' => 'input Email','class' => 'form-control')) !!}
                            @if ($errors->has('Email'))
                                <p class="text-danger">{{ $errors->first('Email') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Client PhoneNo</label>
                        <div class="col-10">
                            {!! Form::text('PhoneNo', null, array('placeholder' => 'input PhoneNo','class' => 'form-control')) !!}
                            @if ($errors->has('PhoneNo'))
                                <p class="text-danger">{{ $errors->first('PhoneNo') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Client Currency</label>
                        <div class="col-10">
                            {!! Form::text('Currency', null, array('placeholder' => 'input Currency','class' => 'form-control')) !!}
                            @if ($errors->has('Currency'))
                                <p class="text-danger">{{ $errors->first('Currency') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Client CurrencySign</label>
                        <div class="col-10">
                            {!! Form::text('CurrencySign', null, array('placeholder' => 'input CurrencySign','class' => 'form-control')) !!}
                            @if ($errors->has('CurrencySign'))
                                <p class="text-danger">{{ $errors->first('CurrencySign') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Client AdsFee</label>
                        <div class="col-10">
                            {!! Form::number('AdsFee', null, array('placeholder' => 'input AdsFee','class' => 'form-control')) !!}
                            @if ($errors->has('AdsFee'))
                                <p class="text-danger">{{ $errors->first('AdsFee') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">IsVerified</label>
                        <div class="col-10">
                            {!! Form::select('IsVerified', ['n'=>'Not Verified','y'=>'Verified'], $listdata->IsVerified,['class' => 'form-control']) !!}
                            @if ($errors->has('IsVerified'))
                                <p class="text-danger">{{ $errors->first('IsVerified') }}</p>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <!-- <label class="col-2 col-form-label" >Last Name</label> -->
                        <div class="col-10">
                            <button class="btn btn-danger waves-effect waves-light"> <i class="fa fa-close m-r-5"></i> <span>Cancel</span> </button>
                            <button class="btn btn-default waves-effect waves-light"> <i class="fa fa-heart m-r-5"></i> <span>Submit</span> </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div> <!-- end row -->

    </div> <!-- container -->

</div> <!-- content -->

@endsection
@section('js')
    <script src="{{asset('plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('plugins/edit_area/edit_area_full.js')}}"></script>
    <script>
        editAreaLoader.init({
            id : "boostrap_class"		// textarea id
            ,syntax: "css"			// syntax to be uses for highgliting
            ,start_highlight: true		// to display with highlight mode on start-up
        });
        if($("#ShortContent").length > 0){
            tinymce.init({
                selector: "textarea#ShortContent",
                theme: "modern",
                height:300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                ]
            });
        }
        if($("#Content").length > 0){
            tinymce.init({
                selector: "textarea#Content",
                theme: "modern",
                height:300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                ]
            });
        }

        function readURL(input, el) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                        $(el).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#upload_file").change(function(){
            var fileName = $(this).val();
            $(this).next('.custom-file-label').html(fileName);
            readURL(this, "#img-preview");
        });
    </script>
@endsection
