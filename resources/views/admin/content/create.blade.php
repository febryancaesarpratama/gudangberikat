@extends('layout.master')
@section('content')

<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Datatable</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Ubold</a></li>
                    <li class="breadcrumb-item"><a href="#">Content</a></li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>

            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card-box table-responsive">
                    <h4 class="m-t-0 header-title">Add Data Content</h4>
                    {!! Form::open(array('route' => 'admin.content.store','method'=>'POST','enctype'=>'multipart/form-data')) !!}
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Title</label>
                        <div class="col-10">
                            {!! Form::text('Title', null, array('placeholder' => 'input Title','class' => 'form-control')) !!}
                            @if ($errors->has('Title'))
                                <p class="text-danger">{{ $errors->first('Title') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Keyword</label>
                        <div class="col-10">
                            {!! Form::textarea('keyword', null, array('placeholder' => 'input keyword','class' => 'form-control','id'=>'keyword')) !!}
                            @if ($errors->has('keyword'))
                                <p class="text-danger">{{ $errors->first('keyword') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Meta-Description</label>
                        <div class="col-10">
                            {!! Form::textarea('meta_description', null, array('placeholder' => 'input meta description','class' => 'form-control','id'=>'meta_description')) !!}
                            @if ($errors->has('meta_description'))
                                <p class="text-danger">{{ $errors->first('meta_description') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">ShortContent</label>
                        <div class="col-10">
                            {!! Form::textarea('ShortContent', null, array('placeholder' => 'input ShortContent','class' => 'form-control','id'=>'ShortContent')) !!}
                            @if ($errors->has('ShortContent'))
                                <p class="text-danger">{{ $errors->first('ShortContent') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Content</label>
                        <div class="col-10">
                            {!! Form::textarea('Content', null, array('placeholder' => 'input Content','class' => 'form-control','id'=>'Content')) !!}
                            @if ($errors->has('Content'))
                                <p class="text-danger">{{ $errors->first('Content') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Prev Article</label>
                        <div class="col-10">
                        {!! Form::select('IDPrevArticle', $listContents, '',['class' => 'form-control']) !!}
                            @if ($errors->has('IDPrevArticle'))
                                <p class="text-danger">{{ $errors->first('IDPrevArticle') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Next Article</label>
                        <div class="col-10">
                            {!! Form::select('IDNextArticle', $listContents, '',['class' => 'form-control']) !!}
                            @if ($errors->has('IDNextArticle'))
                                <p class="text-danger">{{ $errors->first('IDNextArticle') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Upload File</label>
                        <div class="col-6">
                            <input type="file" name="upload_file" id="upload_file" class="form-control">
                        </div>
                        <div class="col-6">
                            <img class="" id="img-preview" src="{{asset('design/img/picture/default.jpg')}}" alt="preview" height="210"> 
                            <p class="mg-t-10 tx-cente tx-13">Image Preview</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Source URL</label>
                        <div class="col-10">
                            {!! Form::text('SourceURL', null, array('placeholder' => 'input SourceURL','class' => 'form-control')) !!}
                            @if ($errors->has('SourceURL'))
                                <p class="text-danger">{{ $errors->first('SourceURL') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Publish Status</label>
                        <div class="col-10">
                        {!! Form::select('IsPublished', ['0' => 'Draft', '1' => 'Publish'], '',['class' => 'form-control']) !!}
                            @if ($errors->has('IsPublished'))
                                <p class="text-danger">{{ $errors->first('IsPublished') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <!-- <label class="col-2 col-form-label" >Last Name</label> -->
                        <div class="col-10">
                            <button class="btn btn-danger waves-effect waves-light"> <i class="fa fa-close m-r-5"></i> <span>Cancel</span> </button>
                            <button class="btn btn-default waves-effect waves-light"> <i class="fa fa-heart m-r-5"></i> <span>Submit</span> </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div> <!-- end row -->

    </div> <!-- container -->

</div> <!-- content -->

@endsection
@section('js')
    <script src="{{asset('plugins/tinymce/tinymce.min.js')}}"></script>
    <script>
        if($("#ShortContent").length > 0){
            tinymce.init({
                selector: "textarea#ShortContent",
                theme: "modern",
                height:300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                ]
            });
        }
        if($("#Content").length > 0){
            tinymce.init({
                selector: "textarea#Content",
                theme: "modern",
                height:300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                ]
            });
        }

        function readURL(input, el) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                        $(el).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#upload_file").change(function(){
            var fileName = $(this).val();
            $(this).next('.custom-file-label').html(fileName);
            readURL(this, "#img-preview");
        });
    </script>
@endsection
