@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-select/css/bootstrap-select.min.css')}}">

@endsection
@section('content')

<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Datatable</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Ubold</a></li>
                    <li class="breadcrumb-item"><a href="#">User</a></li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>

            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card-box table-responsive">
                    <h4 class="m-t-0 header-title">Add Data User</h4>
                    {!! Form::model($listdata, ['method' => 'PATCH','route' => ['admin.user.update', $listdata->id]]) !!}
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Email</label>
                        <div class="col-10">
                            {!! Form::text('email', null, array('placeholder' => 'input email','class' => 'form-control')) !!}
                            @if ($errors->has('email'))
                                <p class="text-danger">{{ $errors->first('email') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Login Name</label>
                        <div class="col-10">
                            {!! Form::text('login_name', null, array('placeholder' => 'input login name','class' => 'form-control')) !!}
                            @if ($errors->has('login_name'))
                                <p class="text-danger">{{ $errors->first('login_name') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label" >Password</label>
                        <div class="col-10">
                        <input type="text" name="password" class="form-control" placeholder="leave empty if you don't want to change">
                            @if ($errors->has('password'))
                                <p class="text-danger">{{ $errors->first('password') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label" >First Name</label>
                        <div class="col-10">
                            {!! Form::text('first_name', null, array('placeholder' => 'input first name','class' => 'form-control')) !!}
                            @if ($errors->has('first_name'))
                                <p class="text-danger">{{ $errors->first('first_name') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label" >Last Name</label>
                        <div class="col-10">
                            {!! Form::text('last_name', null, array('placeholder' => 'input last name','class' => 'form-control')) !!}
                            @if ($errors->has('last_name'))
                                <p class="text-danger">{{ $errors->first('last_name') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-2 col-form-label">Status<span class="tx-danger">*</span></label>
                        <div class="col-10">
                            <select class="form-control" name="is_active">
                                <option value="1" <?php if($listdata->is_active == 1){ echo 'selected="selected"';} ?>>Active</option>
                                <option value="0" <?php if($listdata->is_active == 0){ echo 'selected="selected"';} ?>>Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Role</label>
                        <div class="col-10">
                                {!! Form::select('roles[]', $roles,$userRole, array('class' => 'select2 select2-multiple','multiple')) !!}
                                @if ($errors->has('roles'))
                                    <p class="text-danger">{{ $errors->first('roles') }}</p>
                                @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <!-- <label class="col-2 col-form-label" >Last Name</label> -->
                        <div class="col-10">
                            <button class="btn btn-danger waves-effect waves-light"> <i class="fa fa-close m-r-5"></i> <span>Cancel</span> </button>
                            <button class="btn btn-default waves-effect waves-light"> <i class="fa fa-heart m-r-5"></i> <span>Submit</span> </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div> <!-- end row -->

    </div> <!-- container -->

</div> <!-- content -->

@endsection
@section('js')
<script src="{{asset('plugins/select2/js/select2.min.js')}}"></script>

<script>

$(".select2").select2();
</script>
@endsection