<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Halaman Login
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by login page
    |
    */

    'title_sign' => 'にサインイン',
    'input_username' => 'ユーザー名',
    'input_password' => 'パスワード',
    'success_message' => 'アプリケーションへのログイン成功',
    'fail_message'  => 'ユーザー名またはパスワードが正しくないことを確認してください',
    'remember_me' => '私を覚えてますか',
    'btn_login' => 'ログイン',
    'text_forget_password' => 'パスワードをお忘れですか ？',
    'text_for_register' => "アカウントを持っていませんか？",
    'btn_signup' =>'サインアップ'

];
