<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Halaman Login
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by login page
    |
    */

    'title_sign' => 'Masuk',
    'input_username' => 'Nama user',
    'input_password' => 'Kata sandi',
    'success_message' => 'Berhasil masuk ke aplikasi',
    'fail_message'  => 'Silahkan periksa nama user atau kata sandi anda',
    'remember_me' => 'Ingat saya',
    'btn_login' => 'Masuk',
    'text_forget_password' => 'Lupa kata sandi ?',
    'text_for_register' => "Anda belum punya akun ?. ",
    'btn_signup' =>'Daftar'

];
