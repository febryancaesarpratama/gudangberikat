<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Halaman Login
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by login page
    |
    */

    'title_sign' => 'Sign In to',
    'input_username' => 'Username',
    'input_password' => 'Password',
    'success_message' => 'Success login to aplication',
    'fail_message'  => 'Please check your username or password is incorect',
    'remember_me' => 'Remember Me',
    'btn_login' => 'Login',
    'text_forget_password' => 'Forgot your password ?',
    'text_for_register' => "Don't have an account ?. ",
    'btn_signup' =>'Sign up'

];
