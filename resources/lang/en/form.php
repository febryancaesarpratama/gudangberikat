<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Form Language Lines
    |--------------------------------------------------------------------------
    |
    */
    /*User management*/
    'text_addnew' => 'Add New',
    'text_mandatory_form' => 'sign is mandatory form, it cannot be empty.',
    'text_user_detail' => 'User Details',
    'text_notfound_attribute' => 'Not found any attributes',
    'text_edit' => 'Edit',
    'text_empty_not_save' => 'Empty changes will not be saved',
    'text_listdata' => 'Data list for manage',
    'text_confirmation' => 'Confirmation',
    'text_alert_confirmation' => 'Are you sure you want to remove this data?',
    'text_search' => 'Search...',
    'text_deleting' => 'Deleting...',
    'text_page' => 'Page',
    'text_close' => 'Close',
    'text_filter' => 'Filter',
    'text_alert_date_greater' => 'The start date is greater than the end date',
    'text_alert_complete_date' => 'please complete the date',
    'text_nodata' => 'No Data',

    'button_save' => 'Save',
    'button_edit' => 'Edit',

    'menu_user_management' => 'User Management',
    'menu_permision_management' => 'Permissions Management',
    'menu_role_management' => 'Role Management',
    'menu_user_attribute_type' => 'User Attribute Type',
    'menu_user_attribute' => 'User Attribute',
    'menu_audience_management' => 'Audience Management',
    'menu_manage_badge' => 'Manage Badge',
    'menu_manage_redeem' => 'Manage Redeem',
    'menu_kpitemplate_type' => 'Kpi Template',
    'menu_flexibilitytemplate_type' => 'Flexibility Template',
    'menu_effectivitastemplate_type' => 'Effectivitas Template',
    'menu_personality_report' => 'Personality Report',

];
