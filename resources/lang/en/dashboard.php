<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Halaman Login
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by login page
    |
    */

    'title_card' => 'Dasboard',
    'title_report' => 'Generate Report File for Customs of Republic of Indonesia',
    'label_date_periode_begin'=>'Period',
    'label_date_periode_end'=>'To',
    'btn_view' => 'View',
    'btn_generate' => 'Generate',
    // report transfer of product
        'report_transfer_of_product_no'=>'No',
        'report_transfer_of_product_kode_barang'=>'Item Code',
        'report_transfer_of_product_sat'=>'Unit',
        'report_transfer_of_product_saldo_awal'=>'First Saldo',
        'report_transfer_of_product_pemasukan'=>'Income',
        'report_transfer_of_product_pengeluaran'=>'Outcome',
        'report_transfer_of_product_penyesuaian'=>'Adjustment',
        'report_transfer_of_product_saldo_akhir'=>'Last Saldo',
        'report_transfer_of_product_saldo_selisih'=>'Difference',
        'report_transfer_of_product_saldo_keterangan'=>'Description',
    // report transfer of product
];
