<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Halaman Login
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by login page
    |
    */

    'title_card' => 'Company Info',
    'subtitle' => 'Edit Company Information',
    'label_company_number'=>'Company Number',
    'label_company_name'=>'Company Name',
    'label_company_logo' => 'Company Logo',
    'label_nppkp' => 'Company NPPKP',
    'label_npwp' => 'Company NPWP',
    'label_company_description' => 'Company Description',
    'label_company_address' => 'Company Address',
    'label_company_phone_number' => 'Company Phone Number',
    'label_company_fax' => 'Company Facsimile Number',
    'label_company_email' => 'Company Email Address',
    'btn_save'=>'Save Company Information',
    'msg_success'=> 'Company info has been modified',
    'msg_failed'=>'Company info fail to modify'
];
