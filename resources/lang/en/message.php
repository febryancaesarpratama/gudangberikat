<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'create' => [
        'success' => 'Data is successfully inserted',
        'error' => 'Data is failed to insert',
    ],
    'update' => [
        'success' => 'Data is successfully updated',
        'error' => 'Data is failed to update',
    ],
    'delete' => [
        'success' => 'Data is successfully deleted',
        'error' => 'Data is failed to delete',
    ],
    'error' => '<strong>Whoops! </strong> Something went wrong',
    'have_related' => '<strong>Whoops! </strong> Data cannot be deleted, because have related',
    'not_found' => 'Data not found or has been deleted',
    'success' => 'Success',
    'warning' => 'Warning',
    'error' => 'Error',
    'error_form_below' => 'Please check the form below for errors',

];
