<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Halaman Login
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by login page
    |
    */
    
    'title_sign' => '로그인',
    'input_username' => '사용자 이름',
    'input_password' => '암호',
    'success_message' => '응용 프로그램에 성공 로그인',
    'fail_message'  => '사용자 이름 또는 비밀번호가 incorect인지 확인하십시오.',
    'remember_me' => '날 기억해',
    'btn_login' => '로그인',
    'text_forget_password' => '비밀번호를 잊어 버렸습니까 ?',
    'text_for_register' => "계정이 없습니까?",
    'btn_signup' =>'가입'
];
